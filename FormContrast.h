//---------------------------------------------------------------------------

#ifndef FormContrastH
#define FormContrastH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinTheAsphaltWorld.hpp"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include "main.h"
//---------------------------------------------------------------------------
class TFormCont : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TLabeledEdit *edlAddr;
	TLabeledEdit *edlMask;
	TLabeledEdit *edlValue;
	TcxButton *btnWrite;
	TcxButton *btnRead;
	void __fastcall btnWriteClick(TObject *Sender);
	void __fastcall btnReadClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormCont(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCont *FormCont;
//---------------------------------------------------------------------------
#endif
