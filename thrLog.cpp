//---------------------------------------------------------------------------

#pragma hdrstop

#include "thrLog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
__fastcall Log::Log(const String filename, unsigned int buffer_size) : TThread(false)
{
	Priority = tpLowest;
	_filename = filename;
	_min_buffer_size = buffer_size;
	_buffer = new TStringList;
	Info("Created log");
	_active = true;
	std::ofstream file;
	file.open(_filename.c_str(),std::ofstream::trunc);
	file.close();
	_buffer->LineBreak = "\n";
}

void __fastcall Log::Execute()
{
	while (!Terminated)
	{
		if(_active)
		{
//			while(!_qmsg.empty())
//			{
//				Write(_qmsg.front().first,_qmsg.front().second);
//				_qmsg.pop();
//				_buffer_size++;
//			}
            Sleep(100);
			if(_buffer_size >= _min_buffer_size)
			{
				Synchronize(Release);
			}
		}

    }
}

__fastcall Log::Log(STATUS Status, const String ClassName, const String MethodName, const String Message)
{
	status = Status;
	classname = ClassName;
	methodname = MethodName;
	_buffer ->Add( Message);
}

// ��� ������ �� �������� ������������ ������ ���-�����
void Log::SetClassName(String ClassName)
{
	classname = ClassName;
}

// ��� ������ �� �������� ������������ ������ ���-�����
void Log::SetMethodName(String MethodName)
{
	methodname = MethodName;
}

// ���������, ������� ������������ � ���-����
void Log::SetMessage(String Message)
{
 //	message << Message;
}

// ������ ���������
void Log::SetStatus(STATUS Status)
{
	status = Status;
}

// ������ � ����� ���������
void Log::Write(STATUS Status, String message)
{
	String status_msg = "Unknown";

	if(Status == ST_ERROR)	status_msg = "Error";

	if(Status == ST_WARNING)	status_msg = "Warning";

	if(Status == ST_INFO)	status_msg = "Info";

	if(status_msg != "Unknown")
	{
		SYSTEMTIME tm;
		GetLocalTime(&tm);

		_buffer->Add((AnsiString) IntToStr(tm.wDay) + "/" + IntToStr(tm.wMonth) + "/" + IntToStr(tm.wYear) + "\t"
						+ IntToStr(tm.wHour) + ":" + IntToStr(tm.wMinute) + ":" + IntToStr(tm.wSecond)
						+ "[" + IntToStr(tm.wMilliseconds) + "]" + "\t"
						+ "[" + status_msg + "]\t"
						+ classname + "::"
						+ methodname + " - "
						+ message.c_str()
					);

		_buffer_size++;
	}
}
//---------------------------------------------------------------------------
void Log::Info(const String msg)
{
	try
	{
		Write(ST_INFO,msg);
	}
	catch(Exception &e)
	{
		Write(ST_ERROR,"logger error");
	}

}
//---------------------------------------------------------------------------
void Log::Warning(const String msg)
{
	try
	{
		Write(ST_WARNING,msg);
	}
	catch(Exception &e)
	{
	 	Write(ST_ERROR,"logger error");
	}
}
//---------------------------------------------------------------------------
void Log::Error(const String msg)
{
	try
	{
		Write(ST_ERROR,msg);
	}
	catch(Exception &e)
	{
		Write(ST_ERROR,"logger error");
	}
}
//---------------------------------------------------------------------------
void __fastcall Log::Release()
{
	std::ofstream file;
	file.open(_filename.c_str(),std::ofstream::app);
	file << ((AnsiString)_buffer->GetText()).c_str();
	file.close();
	_buffer->Clear();
  	_buffer_size = 0;
}
//---------------------------------------------------------------------------
void Log::SetMinSize(unsigned int size)
{
	_min_buffer_size = size;
}
