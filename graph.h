#ifndef graphH
#define graphH

#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include <Vcl.Graphics.hpp>
#include <VCLTee.DBChart.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.AppEvnts.hpp>

#include <Math.hpp>		// SimpleRoundTo
#include <list>
#include <string>
#include <algorithm>
#include "thrLog.h"
#include "data.h"

enum GRAPH_MODE { CREATE_REGION, INDICATE,REQUEST_NEXT };
enum GRAPH_EVENT { CREATED, DELETED, UPDATE };
class Region;
class Shape;
class Calculation;
class GraphObserver;

class Graph
{
public:
	Graph(TDBChart * context);

	TDBChart * GetContext() const { return draw_context; }
	void SetMode(GRAPH_MODE m, int t)
	{
		mode = m;
	}
	GRAPH_MODE GetMode() const { return mode; }

	// Mouse Events
	void __fastcall GraphMouseDown(TObject *Sender, TMouseButton Button,TShiftState Shift, int X, int Y);
	void __fastcall GraphMouseUp(TObject *Sender, TMouseButton Button,	TShiftState Shift, int X, int Y);
	void __fastcall GraphMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	// Key events
	void __fastcall GraphKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	//--------------------------------------------------------------------------
	// Regions
	void AddData(RawData * d);
	void AddRegion(Region * r) { regions.push_back(r); }
	void DeleteRegion(Region * r);
	bool SetActiveRegion(Region * r);
	void DeleteActiveRegion();
	void DeleteAllRegions();
	void NewRegionCreated(Region * r);
	void DeactivateRegion();
	//--------------------------------------------------------------------------
	// Shapes
	void AddShape(Shape * s) { shapes.push_back(s); }
	void DeleteShape(Shape * s);
	void NewShapeCreated(Shape * s);
	//--------------------------------------------------------------------------
	// Calculations
	void AddCalculation(Calculation * c) { calculations.push_back(c); }
	void DeleteCalculation(Calculation * c);
	void NewCalculationCreated(Calculation * c);
	//--------------------------------------------------------------------------
	// Observers interface
	void attach(GraphObserver *obs);
	void detach();
	void notify(GRAPH_EVENT e, Region * r);
	void notify(GRAPH_EVENT e, Shape * s);
	void notify(GRAPH_EVENT e, Calculation * c);
	//--------------------------------------------------------------------------
	// Logger
	Log * log;
private:
	TDBChart * draw_context;
	GRAPH_MODE mode;

	std::list<Region*> regions;
	Region * active_region;

	std::list<Shape*> shapes;
	std::list <Calculation*> calculations;

	TPointSeries * _series;

	std::vector<GraphObserver*> observers;
};

#endif
