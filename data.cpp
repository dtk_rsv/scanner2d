#pragma hdrstop
#include "data.h"
//------------------------------------------------------------------------------
/*!
 * \brief Read raw data from txtfile
 * \param[in] filename Path and name of txt file with frame data
 */
bool RawData::ReadFromFile(const std::string & filename)
{
	std::string header;
	std::ifstream file(filename.c_str());
	if(!file)	return false;

	std::locale myloc("");
	file.imbue(myloc);

	std::string time;
	unsigned int n, size, x, y;
	double X;
	double Y;
//	for(int i=0;i<=7;i++) 	//read header
//	{
// 		file >> header;
// 	}

	Clear();	//clear all points
	while(file >> n >> size >> time >> x >> y)
	{

		X=(double)x;
		Y=(double)y;
		Point2D new_point;
		new_point.SetX(X);
		new_point.SetY(Y);
		_points.push_back(new_point);
	}
	_size = _points.size();
	_status = true;
	return true;
}
//------------------------------------------------------------------------------
/*!
 * \brief Get point at requested number
 * \param[in] num Index of the point
 * \return requested point
 */
Point2D RawData::GetPoint(int num) const
{
	Point2D p;
	if(num >=0 && num < _size)
	{
		p = _points[num];
	}
	return p;
}
//------------------------------------------------------------------------------
/*!
 * \brief Get point at requested number
 * \param[in] x X value of the new point
 * \param[in] y Y value of the new point
 */
void RawData::SetPoint(double x, double y)
{
	Point2D point;
	point.SetX(x);
	point.SetY(y);
	_points.push_back(point);
	_size = _points.size();
	_status = true;
}
//------------------------------------------------------------------------------
/*!
 * \brief Delete all points
 */
void RawData::Clear()
{
	_points.clear();
	_size=0;
}
//------------------------------------------------------------------------------
/*!
 * \brief Assign new vector of values
 * \param[in] p Vector of values
 */
void RawData::Assign(std::vector<Point2D> p)
{
	_points.clear();
	_points = p;
	_size = _points.size();
	_status = true;
}
 //------------------------------------------------------------------------------
/*!
 * \brief Append values of given vector into own vector
 * \param[in] p Vector of values
 */
void RawData::Append(std::vector<Point2D> p)
{
	std::copy(p.begin(),p.end(),std::back_inserter(_points));
	_size = _points.size();
	_status = true;
}
#pragma package(smart_init)
