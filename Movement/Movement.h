//---------------------------------------------------------------------------

#ifndef MovementH
#define MovementH
//---------------------------------------------------------------------------
#include "USMCDLL.h"
#include "main.h"
typedef void CallbackUpd_t(Point2D,Point2D);
class Mover : public TThread
{
	public:
		enum THREADMODE {tmFull,tmSerial};
		__fastcall Mover(CallbackUpd_t CallbackUpd);

		USMC_Devices Dvs;
		USMC_State State;
		USMC_StartParameters StPrms;
		USMC_Parameters Prms;
		USMC_Mode Mode;
		USMC_EncoderState EnState;

		bool Connect();
		bool Disconnect();


      	void SetDivisor(int Divisor);
		void Move(unsigned int Channel, double DestPos, double Speed);
		void GetPos(unsigned int Channel, double * PosSteps, double * PosEnc, double * PosReal);
		void SetCircleCenter(const Point2D p);
		void StartMeasure(TMemo* Strings)
		{
			_active = true;
			_points = Strings;
		};
		void StopMeasure() {_active = false;};

	protected:
		void __fastcall Execute();

	private:

		void MoveNext(unsigned int channel);
		void __fastcall UpdResults();



		void (*_callbackResults)(Point2D pCircle, Point2D pTable);
		int	columnEnd;
		int	rowEnd;
		int flStop;
		double _step;
		int _divisor;
		bool _active;
		TMemo * _points;

        int MemLine[2];
		double _encPos[2];

		double _posSteps[2];
		double _posEnc[2];
		double _posReal[2];

		Point2D _posCenterCircle;
		Point2D _posTable;

};
#endif
