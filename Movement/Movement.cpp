//---------------------------------------------------------------------------

#pragma hdrstop

#include "Movement.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma comment(lib,"Psapi.lib")
//#pragma comment(lib,"USMCDLL.lib")
#include <Psapi.h>
//---------------------------------------------------------------------------
/*!
 * \brief Default constructor provided to create the thread suspended
 */
__fastcall Mover::Mover(CallbackUpd_t CallbackUpd) : TThread(false)
{
	_callbackResults = CallbackUpd;
	_step = 0.00015625;
	_divisor = 1;
	_encPos[0]=0;
	_encPos[1]=0;

	MemLine[0] = 0;
	MemLine[1] = 0;
	columnEnd = 0;
	rowEnd = 0;
	flStop = 0;
}
//---------------------------------------------------------------------------
/*!
 * \brief Establish connection with SMC device
 */
bool Mover::Connect()
{

	if(USMC_Init(Dvs))
	{
		ShowMessage("��� ����������� � ��");
		return false;
	}
	else
	{
		USMC_GetMode(0, Mode);
		Mode.PMode=TRUE;
		Mode.ResetD = 0;
		Mode.EncoderEn = TRUE;
		Mode.RotTrOp = FALSE;
		Mode.ResetRT = FALSE;
		Mode.SyncOUTEn=FALSE;
		Mode.Tr1T=FALSE;
		Mode.Tr2T=FALSE;
		Mode.Tr1En=TRUE;
		Mode.Tr2En=TRUE;
		USMC_SetMode(0, Mode);
		USMC_SetMode(1, Mode);

		int newCurPos = 0;
		USMC_SetCurrentPosition(0, newCurPos);
		USMC_SetCurrentPosition(1, newCurPos);
		USMC_GetParameters(0, Prms);
		Prms.MaxTemp = 70.0f;
		Prms.AccelT = 200.0f;
		Prms.DecelT = 200.0f;
		Prms.BTimeout1 = 500.0f;
		Prms.BTimeout2 = 500.0f;
		Prms.BTimeout3 = 500.0f;
		Prms.BTimeout4 = 500.0f;
		Prms.BTO1P = 100.0f;
		Prms.BTO2P = 200.0f;
		Prms.BTO3P = 300.0f;
		Prms.BTO4P = 600.0f;
		Prms.MinP = 500.0f;
		Prms.BTimeoutR = 500.0f;
		Prms.LoftPeriod = 500.0f;
		Prms.RTDelta = 200;
		Prms.RTMinError = 15;
		Prms.EncMult = 5.0;
		Prms.MaxLoft = 32;
		Prms.PTimeout = 100.0f;
		Prms.SynOUTP = 1;
		USMC_SetParameters( 0, Prms );
		USMC_SetParameters( 1, Prms );
	//		BtnConnect->Enabled=false;
	//		BtnDisconnect->Enabled=true;
		return true;
	}

}
//---------------------------------------------------------------------------
/*!
 * \brief Disconnect founded SMC devices
 */
bool Mover::Disconnect()
{
	//wtf is going on? Why?
	if (Dvs.NOD>0)
	{
		USMC_GetState(0,State);
		USMC_GetEncoderState(0,EnState);
//		Edit2->Text=IntToStr(EnState.EncoderPos);
//		Edit1->Text=FloatToStrF((float)StrToFloat(State.CurPos)*STP,ffFixed,10,3);
//		Edit8->Text=FloatToStrF(-((float)(EnState.EncoderPos-enc10)/8000),ffFixed,10,3);

		USMC_GetState(1,State);
		USMC_GetEncoderState(1,EnState);
//		Edit7->Text=IntToStr(EnState.EncoderPos);
//		Edit6->Text=FloatToStrF((float)StrToFloat(State.CurPos)*STP,ffFixed,10,3);
//		Edit9->Text=FloatToStrF(-((float)(EnState.EncoderPos-enc20)/8000),ffFixed,10,3);
	}

}
//---------------------------------------------------------------------------
/*!
 * \brief Disconnect founded SMC devices
 */
void __fastcall Mover::Execute()
{
	TStringList *sl;
	sl= new TStringList;
	static int filecount;
	double Xa[2000];
	double Ya[2000];
	double buff1;
	int changed=0;
	int SBetw=0;
	int HCntr1;
	int i;
	TDateTime timeSt;
	TDateTime timeSt1;

	MemLine[0]=0;
	MemLine[1]=0;
	columnEnd=0;
	rowEnd=0;
	flStop=0;

	USMC_State state[2];
	while(!Terminated)
	{
		if(_active)
		{
			do
			{
				MoveNext(0);
				columnEnd=0;
				do
				{
					MoveNext(1);

					do
					{
						Sleep(1000);
						USMC_GetState(0,state[0]);
						USMC_GetState(1,state[1]);
					}
					while((state[0].RUN)||(state[1].RUN));

					HCntr1=0;

					timeSt=Now();

					do
					{
						//Application->ProcessMessages();
						Sleep(50);
						if (flStop)
						{
							flStop=0;
							return;
						}
						try
						{
							Xa[HCntr1]=_posCenterCircle.x;
							Ya[HCntr1++]=_posCenterCircle.y;
						}
						catch(Exception &e)
						{
						//	ShowMessage(e.ToString());
						}

						timeSt1=Now();
						SBetw=MilliSecondsBetween(timeSt1,timeSt);
					}
					while(SBetw<1000);


					do
					{
						changed=0;
						for (i = 0; i < HCntr1-1; i++)
						{
							if (Xa[i]>Xa[i+1])
							{
								buff1=Xa[i];Xa[i]=Xa[i+1];Xa[i+1]=buff1;
								changed=1;
							}
						}

					} while (changed);

					do
					{
						changed=0;
						for (i = 0; i < HCntr1-1; i++)
						{
							if (Ya[i]>Ya[i+1])
							{
								buff1=Ya[i];Ya[i]=Ya[i+1];Ya[i+1]=buff1;
								changed=1;
							}
						}

					} while (changed);


						Sleep(100);
						GetPos(0,&_posSteps[0],&_posEnc[0],&_posReal[0]);
						GetPos(1,&_posSteps[1],&_posEnc[1],&_posReal[1]);
						_posTable.x = _posReal[0];
						_posTable.y = _posReal[1];
						Synchronize(UpdResults);


						String	filename;
						filename=ExtractFilePath(Application->ExeName)+L"data"+IntToStr(filecount)+".txt";
						String str;

						str +=
							FloatToStrF(_posTable.x,ffGeneral,7,3)+
							FloatToStrF(_posCenterCircle.x,ffGeneral,7,3)+
							FloatToStrF(_posTable.y,ffGeneral,7,3)+
							FloatToStrF(_posCenterCircle.y,ffGeneral,7,3);
						sl->Add(Trim(str));
						str = "\n\r" ;
						sl->SaveToFile(filename);
				}
				while (!columnEnd);
			}
			while ((!columnEnd)||(!rowEnd));
			delete sl;
			filecount++;
		}
	}
}
//---------------------------------------------------------------------------
/*!
 * \brief Move specified drive at the assigned distance
 */
void Mover::Move(unsigned int Channel, double DestPos, double Speed)
{
	if(Dvs.NOD>0)
	{
		double destination = DestPos/_step;  	//doesn't work with direct typecasting
		int dest = (int)destination;
		float speed = Speed;
		USMC_GetStartParameters(Channel,StPrms);
		StPrms.SDivisor=_divisor;
		StPrms.SlStart=TRUE;

		USMC_Start(Channel, dest, speed, StPrms) ;
	}
}
//---------------------------------------------------------------------------
/*!
 * \brief Get the position of specified drive
 */
void Mover::GetPos(unsigned int Channel, double * PosSteps, double * PosEnc, double * PosReal)
{
	double posSteps = 0;
	double posEnc = 0;
	double posReal = 0;
	if(Dvs.NOD>0)
	{
		USMC_GetState(Channel,State);
		USMC_GetEncoderState(Channel,EnState);
		posSteps = State.CurPos * _step;
		posEnc = EnState.EncoderPos;
		posReal = -1*(EnState.EncoderPos-_encPos[Channel]/8000);
		*PosSteps = posSteps;
		*PosEnc = posEnc;
		*PosReal = posReal;
	}
}
//---------------------------------------------------------------------------
/*!
 * \brief Take the next point and move into it
 */
void Mover::MoveNext(unsigned int Channel)
{
	USMC_GetStartParameters(Channel,StPrms);
	StPrms.SDivisor=_divisor;
	StPrms.SlStart=TRUE;
	if (MemLine[Channel]==_points->Lines->Count)
	{
		MemLine[Channel]=0;
		Channel == 0 ? rowEnd=1 : columnEnd=1;
	}

	String temp = _points->Lines->Strings[MemLine[Channel]++];
	if (_points->Lines->Strings[MemLine[Channel]]=="")
	{
		MemLine[Channel]=0;
		Channel == 0 ? rowEnd=1 : columnEnd=1;
	}
	int DestPos = 0;
	try
	{
		DestPos=(int)Ceil((temp.ToDouble())/_step);
	}
	catch(Exception &e)
	{
		MessageBox(NULL,L"Error",e.ToString().w_str(),MB_ICONERROR);
	}
	float Speed= 1000.0f;
	USMC_Start(Channel, DestPos, Speed, StPrms) ;
}
//---------------------------------------------------------------------------
/*!
 * \brief Set the divisor of steps
 */
void Mover::SetDivisor(int Divisor)
{
	_divisor = Divisor;
}
//---------------------------------------------------------------------------
/*!
 * \brief Update results in a form
 */
void __fastcall Mover::UpdResults()
{
	_callbackResults(_posCenterCircle,_posTable);
}
//---------------------------------------------------------------------------
/*!
 * \brief Set the center of calibration circle
 */
void Mover::SetCircleCenter(const Point2D p)
{
	_posCenterCircle = p;
}
