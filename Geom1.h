//---------------------------------------------------------------------------

#ifndef Geom1H
#define Geom1H
//---------------------------------------------------------------------------
#include <math.h>
#include <cfloat>
namespace Geometry
{
	class Point;
	class Edge;
	class Node;
	class Vertex;
	class Polygon;
	enum {CLOCKWISE=9, COUNTER_CLOCKWISE};
	double dotProduct(Point &p, Point &q);
	bool pointlnConvexPolygon (Point &s , Polygon &p);
	class Point {
	public:
	  double x;
	  double y;

	  Point(double _x = 0.0, double _y =0.0);

	  Point operator+(Point&);
	  Point operator-(Point&);
	  friend Point operator* (double, Point&);

	  // ���������� ���������y x, ���� � �������� �������
	  // ���������� y������ �������� �, ��� ���������y y ��� ������� 1
	  double operator[] (int);

	  // ��������� �� ����� ?
	  int operator== (Point&);
	  int operator!= (Point&);

	  // ������������������ ������� ���������, ����� � < ����� b,
	  // ���� ���� �.x < b.x, ���� a.x = b.x � �.y < b.y.
	  int operator< (Point&);
	  int operator> (Point&);
	//  ���������� ��������� �� ���� �������� ������������ �������� ������ �����
	  // ������������ �������� ���� ������������, y���������� �� ���������
	  // ����� ������������ �������
	  // enum {LEFT,  RIGHT,  BEYOND,  BEHIND, BETWEEN, ORIGIN, DESTINATION};
	  //       �����, ������, �������, ������, �����,   ������, �����
	  int classify(Point&, Point&);
	  int classify(Edge);  // ����� ������ ���� �����

	  // ���� ����� � �������� ������� ���������
	  // ���������� -1, ���� ����� = (0, 0)
	  double polarAngle(void);

	  double length(void);

	  double distance(Edge&);
	};

	class Node {  //  ���� ���������� �������� ������
	 protected:
	  Node *_next;	// ����� � ������y����y y��y
	  Node *_prev;	// ����� � ���������y����y y��y
	 public:
	  Node (void);
	  virtual ~Node (void);
	  Node *next(void);
	  Node *prev(void);
	  Node *insert(Node*);  // �������� y��� ����� ���y����
	  Node *remove(void);// y������ y��� �� ������, ���������� ��� y��������
	  void splice(Node*); // �������/���������� ��������x �������
	};
	//����� �������� � ������ Node ����� ��������� � ���������� ���������� ������.
	class Vertex: public Node, public  Point
	{
	 public:
	  Vertex (double x, double y);
	  Vertex (Point);
	  Vertex *cw(void);
	  Vertex *ccw(void);
	  Vertex *neighbor (int rotation);
	  Point point (void);
	  Vertex *insert (Vertex* );
	  Vertex *remove (void);
	  void splice (Vertex*);
	  Vertex *split (Vertex*);
	  friend class Polygon;

	};

	class Polygon {
	 private:
	  Vertex *_v;
	  int _size;
	  void resize (void);
	 public:
	  Polygon (void);
	  Polygon (Polygon&);
	  Polygon (Vertex*);
	  ~Polygon (void);
	  Vertex *v(void);
	  int size (void);
	  Point point (void);
	  Edge edge (void);
	  Vertex *cw(void);
	  Vertex *ccw (void);
	  Vertex *neighbor (int rotation);
	  Vertex *advance (int rotation);
	  Vertex *setV (Vertex*);
	  Vertex *insert (Point&);
	  void remove (void);
	  Polygon * split (Vertex*);
	};

	//����� Edge (�����, ����� �����)

	//����� Edge, ����������� ��� ������������� ���x ���� �����x �����. �� ������������ ����y���� �������:
	class Edge
	{
	 public:
	  Point org;
	  Point dest;
	  Edge (Point _org, Point _dest);
	  Edge (void);
	  Edge &rot (void);
	  Edge &flip (void);
	  Point point (double);
	  int intersect (Edge&, double&);
	  int cross (Edge&, double&);
	  bool isVertical(void);
	  double slope(void);
	  double y(double);
	};
}
#endif
