//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsCore"
#pragma link "dxSkinTheAsphaltWorld"
#pragma link "AdvObj"
#pragma link "cxScrollBar"
#pragma resource "*.dfm"
TFormMain *FormMain;

//---------------------------------------------------------------------------
__fastcall TFormMain::TFormMain(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::RadioGroup1Click(TObject *Sender)
{
//todo:change video mode
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::FormClose(TObject *Sender, TCloseAction &Action)
{
	log->Info("Closing form");
	if(thrDataStream)
	{
		log->Info("Disconnecting network clients...");
		thrDataStream->Disconnect();
		while(thrDataStream->IsConnected())
		{
			//Sleep(10);
			Application->ProcessMessages();
		}
		log->Info("Disconnected successfully. Terminating the thread");
		thrDataStream->Terminate();
		delete thrDataStream;
		log->Info("Thread terminated");
	}

}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N1Click(TObject *Sender)
{
	enum{CONNECT,DISCONNECT};
	if(N1->Tag==CONNECT)
	{
		thrDataStream->Connect();
		N1->Caption="�����������";
		N1->Tag=DISCONNECT;
	}
	else
	{
		thrDataStream->Disconnect();
		N1->Caption="���������� �����";
		N1->Tag=CONNECT;
    }
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N2Click(TObject *Sender)
{
	//todo:send stop command
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnAddRegionClick(TObject *Sender)
{
	rgTypeRegion->Enabled=true;			//allow user to choose the type of new region
	regions->SetSelectStatus(true); 	//allow to choose some regions for multiregion
	sgRegions->UnSelectRows(0,sgRegions->RowCount);
	sgRegions->MouseActions->DisjunctRowSelectNoCtrl=true;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::rgTypeRegionClick(TObject *Sender)
{
	vector<Region*> RegionsForMultiRegion;
	TMeasurement *m = new TMeasurement(data,graph);

	if(rgTypeRegion->ItemIndex==Region::UNION)
	{
		regions->GetSelected(RegionsForMultiRegion);
	}
	m->CreateRegion((Region::REGIONTYPE)rgTypeRegion->ItemIndex,RegionsForMultiRegion);

	delete m;
	rgTypeRegion->ItemIndex=-1;
	rgTypeRegion->Enabled=false;
	regions->SetSelectStatus(false);
	sgRegions->MouseActions->DisjunctRowSelectNoCtrl=false;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::FormCreate(TObject *Sender)
{
	CreateDirectory(L".\\Log\\",NULL);
	log = new Log(".\\Log\\View.log",1);
	TIniFile * ini = new TIniFile (".\\config.ini");
	sgRegions->ColWidths[0]=25;
	sgRegions->ColWidths[1]=58;
	sgRegions->ColWidths[2]=210;
	sgRegions->Cells[0][0]="�";
	sgRegions->Cells[1][0]="���";
	sgRegions->Cells[2][0]="���������";

	sgShapes->ColWidths[0]=25;
	sgShapes->ColWidths[1]=58;
	sgShapes->Cells[0][0]="�";
	sgShapes->Cells[1][0]="���";
	sgShapes->Cells[2][0]="���������";
	sgShapes->ColWidths[2]=210;

	sgCalculations->ColWidths[0]=25;
	sgCalculations->ColWidths[1]=110;
	sgCalculations->ColWidths[2]=90;
	sgCalculations->ColWidths[3]=70;
	sgCalculations->Cells[0][0]="�";
	sgCalculations->Cells[1][0]="���";
    sgCalculations->Cells[2][0]="������";
	sgCalculations->Cells[3][0]="���������";

	sgPSD->Cells[1][0]="X Standa";
	sgPSD->Cells[3][0]="Y Standa";
	sgPSD->Cells[0][0]="X Circle";
	sgPSD->Cells[2][0]="Y Circle";

	graph = new Graph(Chart1);
	regions = new TableRegion(graph, FormMain, sgRegions);
	shapes = new TableShape(graph, FormMain, sgShapes);
	calculations = new TableCalculation (graph, FormMain, sgCalculations);
	data = new RawData();

	series = new TPointSeries(graph->GetContext());
	series->ParentChart = graph->GetContext();
	series->SeriesColor = clBlack;
	series->XValues->Order = loNone;
	series->YValues->Order = loNone;

	series->Legend->Visible = false;

	series->Pointer->Visible = true;
	series->Pointer->Style = psCircle;
	series->Pointer->VertSize = 1;
	series->Pointer->HorizSize = 1;

	graph->GetContext()->Axes->Bottom->Maximum = ini->ReadInteger("ChartSize","width",800);
	graph->GetContext()->Axes->Left->Maximum = ini->ReadInteger("ChartSize","height",600);


	thrDataStream = new TDataStream(true,*CallbackFrame,*CallbackVideo,*CallbackException);
	thrDataStream->Priority=tpNormal;
	thrDataStream->Start();
	thrDataStream->SetMode((TDataStream::THREADMODE)PageControl1->ActivePageIndex);

//	mover = new Mover(*CallbackResults);

	delete ini;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N3Click(TObject *Sender)
{
	log->Info("User requested to export frame");
	thrDataStream->Export();
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N4Click(TObject *Sender)
{
	log->Info("User requested to import frame");
	if(!FileOpenDialog1->Execute())
	{
		log->Warning("File was not selected");
    	return;
    }
	AnsiString filename=FileOpenDialog1->FileName;
	if(data->ReadFromFile(filename.c_str()))
	{
		log->Info("The file is read. Adding to graph...");
		try
		{
			graph->AddData(data);
		}
		catch(Exception &e)
		{
			log->Error("Couldn't add data to graph");
			return;
		}
		log->Info("The data was successfully added");
		btnAddRegion->Enabled=true;
		btnAddShape->Enabled=true;
		btnAddMeasurement->Enabled=true;
	}
	else
	{
        log->Error("Couldn't read the file");
    }
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnAddShapeClick(TObject *Sender)
{
	TMeasurement *m;
	enum { ADD, APPLY };
	unsigned int state = btnAddShape->Tag;
	sgShapes->UnSelectRows(0,sgRegions->RowCount);
	sgShapes->MouseActions->DisjunctRowSelectNoCtrl=true;
	if(state == ADD)
	{
		state = APPLY;
		btnAddShape->Caption = "���������";
		btnAddShape->Enabled = false;
		rgTypeShape->Enabled = true;
	}
	else
	{
		m=new TMeasurement(data,graph);
		m->CreateShape((Shape::SHAPETYPE)rgTypeShape->ItemIndex, regions->GetCurrentRegion());
		state = ADD;
		btnAddShape->Caption = "����� ������";
		rgTypeShape->ItemIndex = -1;
		rgTypeShape->Enabled = false;
		delete m;
	}
	btnAddShape->Tag = state;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::rgTypeShapeClick(TObject *Sender)
{
	btnAddShape->Enabled=true;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnAddMeasurementClick(TObject *Sender)
{
	TMeasurement *m;
	String strResults;
	vector <Shape*> selectedShapes;
	enum { ADD, APPLY };
	int state = btnAddMeasurement->Tag;
	sgShapes->UnSelectRows(0,sgRegions->RowCount);
	sgShapes->MouseActions->DisjunctRowSelectNoCtrl=true;
	if(state == ADD)
    {
		sgShapes->UnSelectRows(0,sgShapes->RowCount);
		sgShapes->MouseActions->DisjunctRowSelectNoCtrl=true;
		state = APPLY;
		btnAddMeasurement->Caption = "���������";
		btnAddMeasurement->Enabled = false;
		rgTypeMeasurement->Enabled = true;
		shapes->SetSelectStatus(true);
	}
	else
	{
		sgShapes->MouseActions->DisjunctRowSelectNoCtrl=false;
		shapes->GetSelected(selectedShapes);

		m=new TMeasurement(data,graph);
		m->Calculate((Calculation::CALCTYPE)rgTypeMeasurement->ItemIndex,selectedShapes);
		delete m;

		state = ADD;
		btnAddMeasurement->Caption = "����� ���������";
		rgTypeMeasurement->ItemIndex = -1;
		rgTypeMeasurement->Enabled = false;
		shapes->SetSelectStatus(false);
	}
	btnAddMeasurement->Tag = state;
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::rgTypeMeasurementClick(TObject *Sender)
{
	btnAddMeasurement->Enabled=true;
}
//---------------------------------------------------------------------------
void CallbackFrame(vector<double> xValues, vector<double> yValues,vector<Point2D> xyValues, int type)
{
	FormMain->log->Info("CallbackFrame called. Updating chart and calculations");
	TMeasurement *m;
	switch (type)
	{
		case TDataStream::FT_FULL:
			FormMain->log->Info("Full frame received");
			m=new TMeasurement(FormMain->data,FormMain->graph);

			FormMain->series->Clear();                      			//clear series for drawing new frame
			FormMain->data->Assign(xyValues);              				//refresh data
			m->CalculateAll(FormMain->calculations,FormMain->shapes);	//make all approximations and calculations
			delete m;

			FormMain->series->AddArray(&xValues.front(), xValues.size()-1, &yValues.front(), yValues.size()-1);

//rsv:to calibrate the sensor (isn't beautiful at all, but must work)
//			Shape * calibCircle = FormMain->shapes->GetCurrentShape();
//			vector<double> results;
//			int type=0;
//			if(calibCircle)
//			{
//				calibCircle->GetRegionAndShapeType(&type);
//				if(type==Shape::CIRCLE)
//				{
//					Point2D p;
//					calibCircle->GetResults(results);
//					p.x = results[0];
//					p.y = results[1];
//					FormMain->mover->SetCircleCenter(p);
//				}
//			}

			break;

//rsv:this cases aren't use
//		case TDataStream::FT_FIRST:
//			FormMain->log->Info("First fragment received");
//
//			m=new TMeasurement(FormMain->data,FormMain->graph);
//
//			FormMain->series->Clear();                      			//clear series for drawing new frame
//			FormMain->data->Append(xyValues);              				//refresh data
//			m->CalculateAll(FormMain->calculations,FormMain->shapes);	//make all approximations and calculations
//			delete m;
//            FormMain->data->Clear();
//			FormMain->series->AddArray(&xValues.front(), xValues.size()-1, &yValues.front(), yValues.size()-1);
//			FormMain->Chart1->Refresh();                      			//update chart
//			break;
//
//		case TDataStream::FT_FRAGMENT:
//			FormMain->log->Info("Fragment received");
//
//			FormMain->data->Append(xyValues);              				//refresh data
//			FormMain->series->AddArray(&xValues.front(), xValues.size()-1, &yValues.front(), yValues.size()-1);
//
//			FormMain->Chart1->Refresh();                      			//update chart
//			break;
    }

}
//---------------------------------------------------------------------------
void CallbackVideo(TBitmap & bmp)
{
	FormMain->log->Info("CallbackVideo called. Updating video frame");
	FormMain->Image1->Canvas->Draw(0,0,&bmp);
}
//---------------------------------------------------------------------------
void CallbackException(Exception & e)
{
	FormMain->log->Info("CallbackException called");
 	Application->ShowException(&e);
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::PageControl1Change(TObject *Sender)
{
	int idx;
	if(PageControl1->ActivePageIndex == 0)
	{
		idx=0;
	}
	else
	{
		idx=PageControl1->ActivePageIndex+1;
    }
	thrDataStream->SetMode((TDataStream::THREADMODE)PageControl1->ActivePageIndex);
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N5Click(TObject *Sender)
{
	N5->Checked = !N5->Checked;
	if(N5->Checked)
	{
		log->Info("User changed auxilary calculations visibility to true");
		calculations->ShowAux();
	}
	else
	{
		log->Info("User changed auxilary calculations visibility to false");
		calculations->HideAux();
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N6Click(TObject *Sender)
{
	N6->Checked = !N6->Checked;
	if(N6->Checked)
	{
		calculations->ShowMarks();
	}
	else
	{
		calculations->HideMarks();
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N8Click(TObject *Sender)
{
	N8->Checked = !N8->Checked;
	if(N8->Checked)
	{
		log->Info("User changed regions visibility to true");
		regions->Show();
	}
	else
	{
		log->Info("User changed regions visibility to false");
		regions->Hide();
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N9Click(TObject *Sender)
{
	N9->Checked = !N9->Checked;
	if(N9->Checked)
	{
		log->Info("User changed shapes visibility to true");
		shapes->Show();
	}
	else
	{
		log->Info("User changed shapes visibility to false");
		shapes->Hide();
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N7Click(TObject *Sender)
{
	N7->Checked = !N7->Checked;
	if(N7->Checked)
	{
		log->Info("User changed ShowClicked option for shapes to true");
		regions->ShowClicked(true);
	}
	else
	{
		log->Info("User changed ShowClicked option for shapes to false");
		regions->ShowClicked(false);
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N10Click(TObject *Sender)
{
	N10->Checked = !N10->Checked;
	if(N10->Checked)
	{
		log->Info("User changed ShowClicked option for shapes to true");
		shapes->ShowClicked(true);
	}
	else
	{
		log->Info("User changed ShowClicked option for shapes to false");
		shapes->ShowClicked(false);
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N12Click(TObject *Sender)
{
// register map is not ready yet
//	FormSettings->Show();
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::N13Click(TObject *Sender)
{
	FormTerm->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cxScrollBar1Change(TObject *Sender)
{
	edlGradient->Text = IntToStr(cxScrollBar1->Position);
	NetCmd cmd("setgl"+FormTerm->PrepareString(edlGradient->Text,2));
	thrDataStream->Send(cmd);
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::edlGradientKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_RETURN)
	{
//		thrDataStream->SetParameter(TDataStream::TP_GRADIENT,cxScrollBar1->Position);
		NetCmd cmd("setgl"+FormTerm->PrepareString(edlGradient->Text,4));
		thrDataStream->Send(cmd);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::cxScrollBar2Change(TObject *Sender)
{
	edlBufferSize->Text = IntToStr(cxScrollBar2->Position);
	edlBufferSize->Text = IntToStr(cxScrollBar2->Position);
	thrDataStream->SetParameter(TDataStream::TP_BUFFERSIZE,edlBufferSize->Text.ToInt());
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::edlBufferSizeKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)

{
	if(Key == VK_RETURN)
	{
		thrDataStream->SetParameter(TDataStream::TP_BUFFERSIZE,edlBufferSize->Text.ToInt());
	}
}
//---------------------------------------------------------------------------



void __fastcall TFormMain::N14Click(TObject *Sender)
{
 	FormCont->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btnConnectSmcClick(TObject *Sender)
{
//	mover->Connect();
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::Button7Click(TObject *Sender)
{
//	mover->Move(0,edlDest1->Text.ToDouble(),edlSpeed1->Text.ToDouble());
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Button1Click(TObject *Sender)
{
//	mover->Move(1,edlDest2->Text.ToDouble(),edlSpeed2->Text.ToDouble());
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnReadPosClick(TObject *Sender)
{
	double steps = 0;
	double enc = 0;
	double real = 0;
//	mover->GetPos(0,&steps,&enc,&real);
	edlPosSteps1->Text = FloatToStrF(steps,ffGeneral,6,3);
	edlPosEnc1->Text = FloatToStrF(enc,ffGeneral,6,3);
	edlPosReal1->Text = FloatToStrF(real,ffGeneral,6,3);
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnReadPos2Click(TObject *Sender)
{
	double steps = 0;
	double enc = 0;
	double real = 0;
//	mover->GetPos(1,&steps,&enc,&real);
	edlPosSteps2->Text = FloatToStrF(steps,ffGeneral,6,3);
	edlPosEnc2->Text = FloatToStrF(enc,ffGeneral,6,3);
	edlPosReal2->Text = FloatToStrF(real,ffGeneral,6,3);
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::btnStartClick(TObject *Sender)
{
	sgPSD->RowCount = 2;
//	mover->StartMeasure(Memo1);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::btnStopClick(TObject *Sender)
{
//	mover->StopMeasure();
}
//---------------------------------------------------------------------------
void __fastcall TFormMain::edlDivisorKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if(Key == VK_RETURN)
	{
//		mover->SetDivisor(edlDivisor->Text.ToInt());
    }
}
//---------------------------------------------------------------------------
void CallbackResults(Point2D pCircle, Point2D pTable)
{
 //						stroka_tabl++;
//						Form2->StringGrid1->Cells[stolbec_tabl][stroka_tabl] = (Form2->Edit10->Text);
//						stolbec_tabl++;
//						Form2->StringGrid1->Cells[stolbec_tabl][stroka_tabl] = (Form2->Edit9->Text);
//						stolbec_tabl++;
//						Form2->StringGrid1->Cells[stolbec_tabl][stroka_tabl] = (Form2->Edit11->Text);
//						stolbec_tabl++;
//						Form2->StringGrid1->Cells[stolbec_tabl][stroka_tabl] = (Form2->Edit8->Text);
//						stolbec_tabl=0;
//						Form2->StringGrid1->RowCount++;
	FormMain->sgPSD->Cells[0][FormMain->sgPSD->RowCount] = FloatToStrF(pTable.x,ffGeneral,7,3);
	FormMain->sgPSD->Cells[1][FormMain->sgPSD->RowCount] = FloatToStrF(pCircle.x,ffGeneral,7,3);
	FormMain->sgPSD->Cells[2][FormMain->sgPSD->RowCount] = FloatToStrF(pTable.y,ffGeneral,7,3);
	FormMain->sgPSD->Cells[3][FormMain->sgPSD->RowCount] = FloatToStrF(pCircle.y,ffGeneral,7,3);
	FormMain->sgPSD->RowCount++;
}
