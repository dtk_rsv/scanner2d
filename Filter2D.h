#ifndef Filter2DH
#define Filter2DH
#include <deque>
#include <vector>
#include <algorithm>
#include <boost/format.hpp>

class Filter2D
{
public:
	Filter2D(size_t wnd_sz);
	void Set(double value);
	double Get() const;
	std::string GetStr() const;

private:
	size_t wnd_size;
	std::deque<double> data;
};
#endif
