//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.DBChart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeSpline.hpp>

#include <IdUDPClient.hpp>
#include <IniFiles.hpp>
#include <Vcl.ComCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinTheAsphaltWorld.hpp"
#include <Vcl.Menus.hpp>
#include "AdvCurve.hpp"
#include "AdvObj.hpp"
#include "BaseGrid.hpp"
#include "advobj.hpp"
#include "AdvGrid.hpp"
#include <Vcl.Grids.hpp>
#include <Vcl.Dialogs.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include "cxScrollBar.hpp"

//stl includes
using namespace std;
#include <vector>
#include <queue>
#include <map>
#include <algorithm>
#include <iostream>
#include <iterator>
//#include <multimap>

//boost
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

//alglib
//using namespace alglib;
#include <ap.h>
#include <interpolation.h>
#include <LinAlg.h>
#include <alglibinternal.h>
#include <statistics.h>

//include project files
#include "Filter.h"
#include "NetCommands.h"
#include "Calibration.h"
#include "data.h"
#include "regions.h"
#include "point.h"
#include "calculations.h"
#include "graph.h"
#include "graph_observers.h"
#include "shapes.h"
#include "thrDataStream.h"
#include "Measurements.h"
#include "FormReg.h"
#include "FormTerminal.h"
#include "FormContrast.h"
#include "Movement/Movement.h"

//boost
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

//log
#include "thrLog.h"

//---------------------------------------------------------------------------
class TableShape;
class TableRegion;
class Graph;
class RawData;
class Mover;
class TFormMain : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *mm1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *N4;
	TFileOpenDialog *FileOpenDialog1;
	TTeeGDIPlus *TeeGDIPlus1;
	TMenuItem *N5;
	TMenuItem *N6;
	TMenuItem *N8;
	TMenuItem *N9;
	TMenuItem *N7;
	TMenuItem *N10;
	TMenuItem *N11;
	TMenuItem *N12;
	TMenuItem *N13;
	TPanel *Panel1;
	TPageControl *PageControl1;
	TTabSheet *ts1;
	TSplitter *Splitter1;
	TPanel *Panel4;
	TDBChart *Chart1;
	TLineSeries *Series1;
	TPanel *Panel2;
	TPageControl *pgc1;
	TTabSheet *ts2;
	TSplitter *Splitter2;
	TSplitter *Splitter4;
	TGroupBox *GroupBox3;
	TRadioGroup *rgTypeRegion;
	TcxButton *btnAddRegion;
	TAdvStringGrid *sgRegions;
	TGroupBox *GroupBox4;
	TRadioGroup *rgTypeShape;
	TcxButton *btnAddShape;
	TAdvStringGrid *sgShapes;
	TGroupBox *GroupBox5;
	TRadioGroup *rgTypeMeasurement;
	TcxButton *btnAddMeasurement;
	TAdvStringGrid *sgCalculations;
	TTabSheet *ts3;
	TLabeledEdit *edlGradient;
	TcxScrollBar *cxScrollBar1;
	TLabeledEdit *edlBufferSize;
	TcxScrollBar *cxScrollBar2;
	TTabSheet *TabSheet1;
	TSplitter *spl2;
	TPanel *Panel5;
	TRadioGroup *RadioGroup1;
	TPanel *Panel6;
	TImage *Image1;
	TMenuItem *N14;
	TTabSheet *TabSheet2;
//	TGroupBox *grp1;
	TButton *Button7;
	TGroupBox *GroupBox1;
	TButton *Button1;
	TLabeledEdit *edlDest1;
	TLabeledEdit *edlSpeed1;
	TLabeledEdit *edlSpeed2;
	TLabeledEdit *edlDest2;
	TGroupBox *GroupBox2;
	TLabeledEdit *edlPosSteps1;
	TLabeledEdit *edlPosEnc1;
	TLabeledEdit *edlPosReal1;
	TGroupBox *GroupBox6;
	TLabeledEdit *edlPosSteps2;
	TLabeledEdit *edlPosEnc2;
	TLabeledEdit *edlPosReal2;
	TButton *btnReadPos;
	TButton *btnReadPos2;
	TPanel *Panel3;
	TcxButton *btnConnectSmc;
	TLabeledEdit *edlDivisor;
	TGroupBox *GroupBox7;
	TMemo *Memo1;
	TGroupBox *GroupBox8;
	TStringGrid *sgPSD;
	TcxButton *btnStart;
	TcxButton *btnStop;
	void __fastcall RadioGroup1Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall btnAddRegionClick(TObject *Sender);
	void __fastcall rgTypeRegionClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall N3Click(TObject *Sender);
	void __fastcall N4Click(TObject *Sender);
	void __fastcall btnAddShapeClick(TObject *Sender);
	void __fastcall rgTypeShapeClick(TObject *Sender);
	void __fastcall btnAddMeasurementClick(TObject *Sender);
	void __fastcall rgTypeMeasurementClick(TObject *Sender);
	void __fastcall PageControl1Change(TObject *Sender);
	void __fastcall N5Click(TObject *Sender);
	void __fastcall N6Click(TObject *Sender);
	void __fastcall N8Click(TObject *Sender);
	void __fastcall N9Click(TObject *Sender);
	void __fastcall N7Click(TObject *Sender);
	void __fastcall N10Click(TObject *Sender);
	void __fastcall N12Click(TObject *Sender);
	void __fastcall N13Click(TObject *Sender);
	void __fastcall cxScrollBar1Change(TObject *Sender);
	void __fastcall edlGradientKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall cxScrollBar2Change(TObject *Sender);
	void __fastcall edlBufferSizeKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall N14Click(TObject *Sender);
	void __fastcall btnConnectSmcClick(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall btnReadPosClick(TObject *Sender);
	void __fastcall btnReadPos2Click(TObject *Sender);
	void __fastcall btnStartClick(TObject *Sender);
	void __fastcall btnStopClick(TObject *Sender);
	void __fastcall edlDivisorKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);






private:	// User declarations


public:		// User declarations
	__fastcall TFormMain(TComponent* Owner);

	//thread callbacks
	friend void CallbackFrame (vector<double>, vector<double>, vector<Point2D>,int);
	friend void CallbackVideo (TBitmap&);
	friend void CallbackException (Exception&);
	friend void CallbackResults(Point2D,Point2D);

	//common objects
    Graph * graph;
	TableRegion * regions;
	TableShape * shapes;
	TableCalculation * calculations;
	RawData * data;
	TCustomSeries * series;
	Log * log;
//    Mover * mover;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
