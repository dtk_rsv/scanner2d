//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormTerminal.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsCore"
#pragma link "dxSkinTheAsphaltWorld"
#pragma resource "*.dfm"
TFormTerm *FormTerm;
//---------------------------------------------------------------------------
__fastcall TFormTerm::TFormTerm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormTerm::btnWriteClick(TObject *Sender)
{
	String sRequest;
	String sTemp;
	sRequest = 	PrepareString(edlAddr->Text,4) +
				PrepareString(edlMask->Text,4) +
				PrepareString(edlValue->Text,4);
	thrDataStream->Write("confset"+sRequest);
	redtTerminal->Lines->Add("[write] " + sRequest);
}
//---------------------------------------------------------------------------
void __fastcall TFormTerm::btnReadClick(TObject *Sender)
{
	String sResponse;
	sResponse = thrDataStream->Read("confget"+PrepareString(edlAddr->Text,4));
	redtTerminal->Lines->Add("[read]  " + sResponse);
	edlValue->Text = sResponse.SubString(8,4);
}
//---------------------------------------------------------------------------
String TFormTerm::PrepareString(String s, int len)
{
	String sTemp;
	if(s.SubString(1,2)=="0x")		//check if value is hexadecimal
	{
		sTemp = s.SubString(3,len);
	}
	else                           //otherwise convert into hex
	{
		sTemp = IntToHex(s.ToInt(),len);
	}

	while(sTemp.Length() < len)
	{
		sTemp.Insert("0",0);
	}
	return sTemp;
}
