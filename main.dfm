object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'D-Test Scanner'
  ClientHeight = 710
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 710
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 1262
      Height = 708
      ActivePage = ts1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object ts1: TTabSheet
        Caption = #1056#1077#1078#1080#1084' '#1080#1079#1084#1077#1088#1077#1085#1080#1081
        object Splitter1: TSplitter
          Left = 789
          Top = 0
          Height = 680
          Align = alRight
          ExplicitLeft = 776
          ExplicitHeight = 722
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 789
          Height = 680
          Align = alClient
          Caption = 'Panel4'
          TabOrder = 0
          object Chart1: TDBChart
            Left = 1
            Top = 1
            Width = 787
            Height = 678
            Border.Fill.Gradient.Colors = <
              item
                Color = clWhite
                Transparency = 100
              end
              item
                Color = clYellow
                Offset = 1.000000000000000000
              end>
            Border.Fill.Gradient.Visible = True
            Gradient.Colors = <
              item
                Color = 15656669
              end
              item
                Color = clWhite
                Offset = 0.467065868263473200
              end
              item
                Color = clWhite
                Offset = 1.000000000000000000
              end>
            Gradient.EndColor = clWhite
            Gradient.MidColor = clWhite
            Gradient.StartColor = 15656669
            Gradient.Visible = True
            Title.ClipText = False
            Title.Font.Color = 939524096
            Title.Font.Height = -16
            Title.Font.Name = 'Tahoma'
            Title.Margins.Top = 44
            Title.Margins.Bottom = 28
            Title.Shadow.HorizSize = 8
            Title.Shadow.VertSize = 8
            Title.Text.Strings = (
              #1055#1088#1086#1092#1080#1083#1100' '#1076#1077#1090#1072#1083#1080)
            Title.Visible = False
            Title.VertMargin = 4
            BottomAxis.Automatic = False
            BottomAxis.AutomaticMaximum = False
            BottomAxis.AutomaticMinimum = False
            BottomAxis.Inverted = True
            BottomAxis.LabelsOnAxis = False
            BottomAxis.LabelStyle = talValue
            BottomAxis.Maximum = 600.000000000000000000
            BottomAxis.Title.Caption = 'X'
            BottomAxis.Title.Font.Height = -13
            BottomAxis.Title.Font.Name = 'Tahoma'
            LeftAxis.Automatic = False
            LeftAxis.AutomaticMaximum = False
            LeftAxis.AutomaticMinimum = False
            LeftAxis.Inverted = True
            LeftAxis.LabelsSize = 30
            LeftAxis.Maximum = 800.000000000000000000
            LeftAxis.Title.Caption = 'Z'
            LeftAxis.Title.Font.Height = -13
            LeftAxis.Title.Font.Name = 'Tahoma'
            LeftAxis.Title.Font.Quality = fqClearType
            LeftAxis.Title.Pen.Width = 0
            Legend.Alignment = laTop
            Legend.CheckBoxes = True
            Legend.Visible = False
            View3D = False
            Align = alClient
            Color = clGray
            TabOrder = 0
            DefaultCanvas = 'TGDIPlusCanvas'
            ColorPaletteIndex = 13
            object Series1: TLineSeries
              Marks.Children = <
                item
                  Shape.ShapeStyle = fosRectangle
                  Shape.Style = smsValue
                end>
              Marks.Font.Quality = fqBest
              Marks.RoundSize = 1
              Marks.Visible = True
              Marks.Arrow.Color = clDefault
              Marks.Arrow.Style = psDash
              Marks.Arrow.Width = 53
              Marks.Arrow.EndStyle = esSquare
              Marks.Arrow.Fill.Gradient.Visible = True
              Marks.Arrow.JoinStyle = jsMitter
              Marks.Arrow.Visible = False
              Marks.Callout.Arrow.Color = clDefault
              Marks.Callout.Arrow.Style = psDash
              Marks.Callout.Arrow.Width = 53
              Marks.Callout.Arrow.EndStyle = esSquare
              Marks.Callout.Arrow.Fill.Gradient.Visible = True
              Marks.Callout.Arrow.JoinStyle = jsMitter
              Marks.Callout.Arrow.Visible = False
              Marks.Callout.ArrowHead = ahSolid
              Marks.Callout.Distance = -21
              Marks.Symbol.Frame.Visible = False
              Marks.Symbol.Pen.Visible = False
              Brush.BackColor = clDefault
              Pointer.InflateMargins = True
              Pointer.Style = psRectangle
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.Order = loNone
            end
          end
        end
        object Panel2: TPanel
          Left = 792
          Top = 0
          Width = 462
          Height = 680
          Align = alRight
          Caption = 'Panel2'
          TabOrder = 1
          object pgc1: TPageControl
            Left = 1
            Top = 1
            Width = 460
            Height = 678
            ActivePage = ts2
            Align = alClient
            DoubleBuffered = False
            MultiLine = True
            ParentDoubleBuffered = False
            TabOrder = 0
            object ts2: TTabSheet
              Caption = #1048#1079#1084#1077#1088#1077#1085#1080#1103
              object Splitter2: TSplitter
                Left = 0
                Top = 204
                Width = 452
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitTop = 224
                ExplicitWidth = 195
              end
              object Splitter4: TSplitter
                Left = 0
                Top = 426
                Width = 452
                Height = 3
                Cursor = crVSplit
                Align = alBottom
                ExplicitTop = 410
              end
              object GroupBox3: TGroupBox
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 446
                Height = 198
                Align = alTop
                Caption = #1056#1077#1075#1080#1086#1085#1099
                TabOrder = 0
                DesignSize = (
                  446
                  198)
                object rgTypeRegion: TRadioGroup
                  AlignWithMargins = True
                  Left = 325
                  Top = 50
                  Width = 110
                  Height = 143
                  Anchors = [akTop, akRight, akBottom]
                  Caption = #1058#1080#1087' '#1088#1077#1075#1080#1086#1085#1072
                  Enabled = False
                  Items.Strings = (
                    #1055#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1080#1082
                    #1058#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082
                    #1057#1074#1086#1073#1086#1076#1085#1086#1077' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
                    #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1077)
                  TabOrder = 0
                  WordWrap = True
                  OnClick = rgTypeRegionClick
                end
                object btnAddRegion: TcxButton
                  AlignWithMargins = True
                  Left = 325
                  Top = 19
                  Width = 111
                  Height = 25
                  Anchors = [akTop, akRight]
                  Caption = #1053#1086#1074#1099#1081' '#1088#1077#1075#1080#1086#1085
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 1
                  OnClick = btnAddRegionClick
                end
                object sgRegions: TAdvStringGrid
                  AlignWithMargins = True
                  Left = 5
                  Top = 18
                  Width = 314
                  Height = 175
                  Cursor = crDefault
                  Align = alLeft
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  ColCount = 3
                  DrawingStyle = gdsClassic
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSelect, goFixedRowClick]
                  ScrollBars = ssBoth
                  TabOrder = 2
                  ActiveRowShow = True
                  GridLineColor = 15527152
                  GridFixedLineColor = 13947601
                  HoverRowCells = [hcNormal, hcSelected]
                  ActiveCellFont.Charset = DEFAULT_CHARSET
                  ActiveCellFont.Color = clWindowText
                  ActiveCellFont.Height = -11
                  ActiveCellFont.Name = 'Tahoma'
                  ActiveCellFont.Style = [fsBold]
                  ActiveCellColor = 16575452
                  ActiveCellColorTo = 16571329
                  ControlLook.FixedGradientMirrorFrom = 16049884
                  ControlLook.FixedGradientMirrorTo = 16247261
                  ControlLook.FixedGradientHoverFrom = 16710648
                  ControlLook.FixedGradientHoverTo = 16446189
                  ControlLook.FixedGradientHoverMirrorFrom = 16049367
                  ControlLook.FixedGradientHoverMirrorTo = 15258305
                  ControlLook.FixedGradientDownFrom = 15853789
                  ControlLook.FixedGradientDownTo = 15852760
                  ControlLook.FixedGradientDownMirrorFrom = 15522767
                  ControlLook.FixedGradientDownMirrorTo = 15588559
                  ControlLook.FixedGradientDownBorder = 14007466
                  ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownHeader.Font.Color = clWindowText
                  ControlLook.DropDownHeader.Font.Height = -11
                  ControlLook.DropDownHeader.Font.Name = 'Tahoma'
                  ControlLook.DropDownHeader.Font.Style = []
                  ControlLook.DropDownHeader.Visible = True
                  ControlLook.DropDownHeader.Buttons = <>
                  ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownFooter.Font.Color = clWindowText
                  ControlLook.DropDownFooter.Font.Height = -11
                  ControlLook.DropDownFooter.Font.Name = 'Tahoma'
                  ControlLook.DropDownFooter.Font.Style = []
                  ControlLook.DropDownFooter.Visible = True
                  ControlLook.DropDownFooter.Buttons = <>
                  Filter = <
                    item
                      Column = 0
                      Operation = foSHORT
                      Method = fmExpression
                    end>
                  FilterDropDown.Font.Charset = DEFAULT_CHARSET
                  FilterDropDown.Font.Color = clWindowText
                  FilterDropDown.Font.Height = -11
                  FilterDropDown.Font.Name = 'Tahoma'
                  FilterDropDown.Font.Style = []
                  FilterDropDown.TextChecked = 'Checked'
                  FilterDropDown.TextUnChecked = 'Unchecked'
                  FilterDropDownClear = '(All)'
                  FilterEdit.TypeNames.Strings = (
                    'Starts with'
                    'Ends with'
                    'Contains'
                    'Not contains'
                    'Equal'
                    'Not equal'
                    'Larger than'
                    'Smaller than'
                    'Clear')
                  FixedRowHeight = 22
                  FixedFont.Charset = DEFAULT_CHARSET
                  FixedFont.Color = clBlack
                  FixedFont.Height = -11
                  FixedFont.Name = 'Tahoma'
                  FixedFont.Style = [fsBold]
                  FloatFormat = '%.2f'
                  HoverButtons.Buttons = <>
                  HoverButtons.Position = hbLeftFromColumnLeft
                  HTMLSettings.ImageFolder = 'images'
                  HTMLSettings.ImageBaseName = 'img'
                  Look = glWin7
                  MouseActions.DisjunctRowSelect = True
                  MouseActions.WheelAction = waScroll
                  Multilinecells = True
                  PrintSettings.DateFormat = 'dd/mm/yyyy'
                  PrintSettings.Font.Charset = DEFAULT_CHARSET
                  PrintSettings.Font.Color = clWindowText
                  PrintSettings.Font.Height = -11
                  PrintSettings.Font.Name = 'Tahoma'
                  PrintSettings.Font.Style = []
                  PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FixedFont.Color = clWindowText
                  PrintSettings.FixedFont.Height = -11
                  PrintSettings.FixedFont.Name = 'Tahoma'
                  PrintSettings.FixedFont.Style = []
                  PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
                  PrintSettings.HeaderFont.Color = clWindowText
                  PrintSettings.HeaderFont.Height = -11
                  PrintSettings.HeaderFont.Name = 'Tahoma'
                  PrintSettings.HeaderFont.Style = []
                  PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FooterFont.Color = clWindowText
                  PrintSettings.FooterFont.Height = -11
                  PrintSettings.FooterFont.Name = 'Tahoma'
                  PrintSettings.FooterFont.Style = []
                  PrintSettings.PageNumSep = '/'
                  SearchFooter.Color = 16645370
                  SearchFooter.ColorTo = 16247261
                  SearchFooter.FindNextCaption = 'Find &next'
                  SearchFooter.FindPrevCaption = 'Find &previous'
                  SearchFooter.Font.Charset = DEFAULT_CHARSET
                  SearchFooter.Font.Color = clWindowText
                  SearchFooter.Font.Height = -11
                  SearchFooter.Font.Name = 'Tahoma'
                  SearchFooter.Font.Style = []
                  SearchFooter.HighLightCaption = 'Highlight'
                  SearchFooter.HintClose = 'Close'
                  SearchFooter.HintFindNext = 'Find next occurrence'
                  SearchFooter.HintFindPrev = 'Find previous occurrence'
                  SearchFooter.HintHighlight = 'Highlight occurrences'
                  SearchFooter.MatchCaseCaption = 'Match case'
                  ShowDesignHelper = False
                  SizeWithForm = True
                  SortSettings.DefaultFormat = ssAutomatic
                  SortSettings.HeaderColor = 16579058
                  SortSettings.HeaderColorTo = 16579058
                  SortSettings.HeaderMirrorColor = 16380385
                  SortSettings.HeaderMirrorColorTo = 16182488
                  Version = '7.9.0.3'
                  ColWidths = (
                    64
                    64
                    64)
                  RowHeights = (
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22)
                end
              end
              object GroupBox4: TGroupBox
                AlignWithMargins = True
                Left = 3
                Top = 210
                Width = 446
                Height = 213
                Align = alClient
                Anchors = [akLeft, akRight]
                Caption = #1060#1080#1075#1091#1088#1099
                TabOrder = 1
                DesignSize = (
                  446
                  213)
                object rgTypeShape: TRadioGroup
                  AlignWithMargins = True
                  Left = 325
                  Top = 50
                  Width = 110
                  Height = 150
                  Anchors = [akTop, akRight, akBottom]
                  Caption = #1058#1080#1087' '#1092#1080#1075#1091#1088#1099
                  Enabled = False
                  Items.Strings = (
                    #1051#1080#1085#1080#1103
                    #1054#1082#1088#1091#1078#1085#1086#1089#1090#1100
                    #1069#1083#1083#1080#1087#1089)
                  TabOrder = 0
                  OnClick = rgTypeShapeClick
                end
                object btnAddShape: TcxButton
                  AlignWithMargins = True
                  Left = 325
                  Top = 19
                  Width = 111
                  Height = 25
                  Anchors = [akTop, akRight]
                  Caption = #1053#1086#1074#1072#1103' '#1092#1080#1075#1091#1088#1072
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 1
                  OnClick = btnAddShapeClick
                end
                object sgShapes: TAdvStringGrid
                  AlignWithMargins = True
                  Left = 5
                  Top = 18
                  Width = 314
                  Height = 190
                  Cursor = crDefault
                  Align = alLeft
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  ColCount = 3
                  DrawingStyle = gdsClassic
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSelect, goFixedRowClick]
                  ScrollBars = ssBoth
                  TabOrder = 2
                  ActiveRowShow = True
                  GridLineColor = 15527152
                  GridFixedLineColor = 13947601
                  HoverRowCells = [hcNormal, hcSelected]
                  ActiveCellFont.Charset = DEFAULT_CHARSET
                  ActiveCellFont.Color = clWindowText
                  ActiveCellFont.Height = -11
                  ActiveCellFont.Name = 'Tahoma'
                  ActiveCellFont.Style = [fsBold]
                  ActiveCellColor = 16575452
                  ActiveCellColorTo = 16571329
                  ControlLook.FixedGradientMirrorFrom = 16049884
                  ControlLook.FixedGradientMirrorTo = 16247261
                  ControlLook.FixedGradientHoverFrom = 16710648
                  ControlLook.FixedGradientHoverTo = 16446189
                  ControlLook.FixedGradientHoverMirrorFrom = 16049367
                  ControlLook.FixedGradientHoverMirrorTo = 15258305
                  ControlLook.FixedGradientDownFrom = 15853789
                  ControlLook.FixedGradientDownTo = 15852760
                  ControlLook.FixedGradientDownMirrorFrom = 15522767
                  ControlLook.FixedGradientDownMirrorTo = 15588559
                  ControlLook.FixedGradientDownBorder = 14007466
                  ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownHeader.Font.Color = clWindowText
                  ControlLook.DropDownHeader.Font.Height = -11
                  ControlLook.DropDownHeader.Font.Name = 'Tahoma'
                  ControlLook.DropDownHeader.Font.Style = []
                  ControlLook.DropDownHeader.Visible = True
                  ControlLook.DropDownHeader.Buttons = <>
                  ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownFooter.Font.Color = clWindowText
                  ControlLook.DropDownFooter.Font.Height = -11
                  ControlLook.DropDownFooter.Font.Name = 'Tahoma'
                  ControlLook.DropDownFooter.Font.Style = []
                  ControlLook.DropDownFooter.Visible = True
                  ControlLook.DropDownFooter.Buttons = <>
                  Filter = <
                    item
                      Column = 0
                      Operation = foSHORT
                      Method = fmExpression
                    end>
                  FilterDropDown.Font.Charset = DEFAULT_CHARSET
                  FilterDropDown.Font.Color = clWindowText
                  FilterDropDown.Font.Height = -11
                  FilterDropDown.Font.Name = 'Tahoma'
                  FilterDropDown.Font.Style = []
                  FilterDropDown.TextChecked = 'Checked'
                  FilterDropDown.TextUnChecked = 'Unchecked'
                  FilterDropDownClear = '(All)'
                  FilterEdit.TypeNames.Strings = (
                    'Starts with'
                    'Ends with'
                    'Contains'
                    'Not contains'
                    'Equal'
                    'Not equal'
                    'Larger than'
                    'Smaller than'
                    'Clear')
                  FixedRowHeight = 22
                  FixedFont.Charset = DEFAULT_CHARSET
                  FixedFont.Color = clBlack
                  FixedFont.Height = -11
                  FixedFont.Name = 'Tahoma'
                  FixedFont.Style = [fsBold]
                  FloatFormat = '%.2f'
                  HoverButtons.Buttons = <>
                  HoverButtons.Position = hbLeftFromColumnLeft
                  HTMLSettings.ImageFolder = 'images'
                  HTMLSettings.ImageBaseName = 'img'
                  Look = glWin7
                  MouseActions.DisjunctRowSelect = True
                  MouseActions.WheelAction = waScroll
                  Multilinecells = True
                  PrintSettings.DateFormat = 'dd/mm/yyyy'
                  PrintSettings.Font.Charset = DEFAULT_CHARSET
                  PrintSettings.Font.Color = clWindowText
                  PrintSettings.Font.Height = -11
                  PrintSettings.Font.Name = 'Tahoma'
                  PrintSettings.Font.Style = []
                  PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FixedFont.Color = clWindowText
                  PrintSettings.FixedFont.Height = -11
                  PrintSettings.FixedFont.Name = 'Tahoma'
                  PrintSettings.FixedFont.Style = []
                  PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
                  PrintSettings.HeaderFont.Color = clWindowText
                  PrintSettings.HeaderFont.Height = -11
                  PrintSettings.HeaderFont.Name = 'Tahoma'
                  PrintSettings.HeaderFont.Style = []
                  PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FooterFont.Color = clWindowText
                  PrintSettings.FooterFont.Height = -11
                  PrintSettings.FooterFont.Name = 'Tahoma'
                  PrintSettings.FooterFont.Style = []
                  PrintSettings.PageNumSep = '/'
                  SearchFooter.Color = 16645370
                  SearchFooter.ColorTo = 16247261
                  SearchFooter.FindNextCaption = 'Find &next'
                  SearchFooter.FindPrevCaption = 'Find &previous'
                  SearchFooter.Font.Charset = DEFAULT_CHARSET
                  SearchFooter.Font.Color = clWindowText
                  SearchFooter.Font.Height = -11
                  SearchFooter.Font.Name = 'Tahoma'
                  SearchFooter.Font.Style = []
                  SearchFooter.HighLightCaption = 'Highlight'
                  SearchFooter.HintClose = 'Close'
                  SearchFooter.HintFindNext = 'Find next occurrence'
                  SearchFooter.HintFindPrev = 'Find previous occurrence'
                  SearchFooter.HintHighlight = 'Highlight occurrences'
                  SearchFooter.MatchCaseCaption = 'Match case'
                  ShowDesignHelper = False
                  SizeWithForm = True
                  SortSettings.DefaultFormat = ssAutomatic
                  SortSettings.HeaderColor = 16579058
                  SortSettings.HeaderColorTo = 16579058
                  SortSettings.HeaderMirrorColor = 16380385
                  SortSettings.HeaderMirrorColorTo = 16182488
                  Version = '7.9.0.3'
                  ColWidths = (
                    64
                    64
                    64)
                  RowHeights = (
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22)
                end
              end
              object GroupBox5: TGroupBox
                AlignWithMargins = True
                Left = 3
                Top = 432
                Width = 446
                Height = 215
                Align = alBottom
                Anchors = [akLeft, akTop, akRight]
                Caption = #1048#1079#1084#1077#1088#1077#1085#1080#1103
                TabOrder = 2
                DesignSize = (
                  446
                  215)
                object rgTypeMeasurement: TRadioGroup
                  AlignWithMargins = True
                  Left = 327
                  Top = 50
                  Width = 110
                  Height = 160
                  Anchors = [akTop, akRight, akBottom]
                  Caption = #1058#1080#1087' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
                  Enabled = False
                  Items.Strings = (
                    #1054#1089#1090#1088#1099#1081' '#1091#1075#1086#1083' '#1084#1077#1078#1076#1091' '#1083#1080#1085#1080#1103#1084#1080
                    #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077' '#1084#1077#1078#1076#1091' '#1094#1077#1085#1090#1088#1072#1084#1080' '#1092#1080#1075#1091#1088
                    #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077' '#1084#1077#1078#1076#1091' '#1083#1080#1085#1080#1103#1084#1080)
                  TabOrder = 0
                  WordWrap = True
                  OnClick = rgTypeMeasurementClick
                end
                object btnAddMeasurement: TcxButton
                  AlignWithMargins = True
                  Left = 327
                  Top = 19
                  Width = 110
                  Height = 25
                  Anchors = [akTop, akRight]
                  Caption = #1053#1086#1074#1086#1077' '#1080#1079#1084#1077#1088#1077#1085#1080#1077
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 1
                  OnClick = btnAddMeasurementClick
                end
                object sgCalculations: TAdvStringGrid
                  AlignWithMargins = True
                  Left = 5
                  Top = 18
                  Width = 316
                  Height = 192
                  Cursor = crDefault
                  Align = alLeft
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  ColCount = 4
                  DrawingStyle = gdsClassic
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSelect, goFixedRowClick]
                  ScrollBars = ssBoth
                  TabOrder = 2
                  ActiveRowShow = True
                  GridLineColor = 15527152
                  GridFixedLineColor = 13947601
                  HoverRowCells = [hcNormal, hcSelected]
                  ActiveCellFont.Charset = DEFAULT_CHARSET
                  ActiveCellFont.Color = clWindowText
                  ActiveCellFont.Height = -11
                  ActiveCellFont.Name = 'Tahoma'
                  ActiveCellFont.Style = [fsBold]
                  ActiveCellColor = 16575452
                  ActiveCellColorTo = 16571329
                  ControlLook.FixedGradientMirrorFrom = 16049884
                  ControlLook.FixedGradientMirrorTo = 16247261
                  ControlLook.FixedGradientHoverFrom = 16710648
                  ControlLook.FixedGradientHoverTo = 16446189
                  ControlLook.FixedGradientHoverMirrorFrom = 16049367
                  ControlLook.FixedGradientHoverMirrorTo = 15258305
                  ControlLook.FixedGradientDownFrom = 15853789
                  ControlLook.FixedGradientDownTo = 15852760
                  ControlLook.FixedGradientDownMirrorFrom = 15522767
                  ControlLook.FixedGradientDownMirrorTo = 15588559
                  ControlLook.FixedGradientDownBorder = 14007466
                  ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownHeader.Font.Color = clWindowText
                  ControlLook.DropDownHeader.Font.Height = -11
                  ControlLook.DropDownHeader.Font.Name = 'Tahoma'
                  ControlLook.DropDownHeader.Font.Style = []
                  ControlLook.DropDownHeader.Visible = True
                  ControlLook.DropDownHeader.Buttons = <>
                  ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
                  ControlLook.DropDownFooter.Font.Color = clWindowText
                  ControlLook.DropDownFooter.Font.Height = -11
                  ControlLook.DropDownFooter.Font.Name = 'Tahoma'
                  ControlLook.DropDownFooter.Font.Style = []
                  ControlLook.DropDownFooter.Visible = True
                  ControlLook.DropDownFooter.Buttons = <>
                  Filter = <
                    item
                      Column = 0
                      Operation = foSHORT
                      Method = fmExpression
                    end>
                  FilterDropDown.Font.Charset = DEFAULT_CHARSET
                  FilterDropDown.Font.Color = clWindowText
                  FilterDropDown.Font.Height = -11
                  FilterDropDown.Font.Name = 'Tahoma'
                  FilterDropDown.Font.Style = []
                  FilterDropDown.TextChecked = 'Checked'
                  FilterDropDown.TextUnChecked = 'Unchecked'
                  FilterDropDownClear = '(All)'
                  FilterEdit.TypeNames.Strings = (
                    'Starts with'
                    'Ends with'
                    'Contains'
                    'Not contains'
                    'Equal'
                    'Not equal'
                    'Larger than'
                    'Smaller than'
                    'Clear')
                  FixedRowHeight = 22
                  FixedFont.Charset = DEFAULT_CHARSET
                  FixedFont.Color = clBlack
                  FixedFont.Height = -11
                  FixedFont.Name = 'Tahoma'
                  FixedFont.Style = [fsBold]
                  FloatFormat = '%.2f'
                  HoverButtons.Buttons = <>
                  HoverButtons.Position = hbLeftFromColumnLeft
                  HTMLSettings.ImageFolder = 'images'
                  HTMLSettings.ImageBaseName = 'img'
                  Look = glWin7
                  MouseActions.DisjunctRowSelect = True
                  MouseActions.WheelAction = waScroll
                  Multilinecells = True
                  PrintSettings.DateFormat = 'dd/mm/yyyy'
                  PrintSettings.Font.Charset = DEFAULT_CHARSET
                  PrintSettings.Font.Color = clWindowText
                  PrintSettings.Font.Height = -11
                  PrintSettings.Font.Name = 'Tahoma'
                  PrintSettings.Font.Style = []
                  PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FixedFont.Color = clWindowText
                  PrintSettings.FixedFont.Height = -11
                  PrintSettings.FixedFont.Name = 'Tahoma'
                  PrintSettings.FixedFont.Style = []
                  PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
                  PrintSettings.HeaderFont.Color = clWindowText
                  PrintSettings.HeaderFont.Height = -11
                  PrintSettings.HeaderFont.Name = 'Tahoma'
                  PrintSettings.HeaderFont.Style = []
                  PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
                  PrintSettings.FooterFont.Color = clWindowText
                  PrintSettings.FooterFont.Height = -11
                  PrintSettings.FooterFont.Name = 'Tahoma'
                  PrintSettings.FooterFont.Style = []
                  PrintSettings.PageNumSep = '/'
                  SearchFooter.Color = 16645370
                  SearchFooter.ColorTo = 16247261
                  SearchFooter.FindNextCaption = 'Find &next'
                  SearchFooter.FindPrevCaption = 'Find &previous'
                  SearchFooter.Font.Charset = DEFAULT_CHARSET
                  SearchFooter.Font.Color = clWindowText
                  SearchFooter.Font.Height = -11
                  SearchFooter.Font.Name = 'Tahoma'
                  SearchFooter.Font.Style = []
                  SearchFooter.HighLightCaption = 'Highlight'
                  SearchFooter.HintClose = 'Close'
                  SearchFooter.HintFindNext = 'Find next occurrence'
                  SearchFooter.HintFindPrev = 'Find previous occurrence'
                  SearchFooter.HintHighlight = 'Highlight occurrences'
                  SearchFooter.MatchCaseCaption = 'Match case'
                  ShowDesignHelper = False
                  SizeWithForm = True
                  SortSettings.DefaultFormat = ssAutomatic
                  SortSettings.HeaderColor = 16579058
                  SortSettings.HeaderColorTo = 16579058
                  SortSettings.HeaderMirrorColor = 16380385
                  SortSettings.HeaderMirrorColorTo = 16182488
                  Version = '7.9.0.3'
                  ColWidths = (
                    64
                    64
                    64
                    64)
                  RowHeights = (
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22
                    22)
                end
              end
            end
            object ts3: TTabSheet
              Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1073#1088#1072#1073#1086#1090#1082#1080
              ImageIndex = 1
              object edlGradient: TLabeledEdit
                Left = 104
                Top = 24
                Width = 89
                Height = 21
                Alignment = taRightJustify
                EditLabel.AlignWithMargins = True
                EditLabel.Width = 55
                EditLabel.Height = 16
                EditLabel.Margins.Right = 5
                EditLabel.Caption = #1043#1088#1072#1076#1080#1077#1085#1090
                EditLabel.Font.Charset = DEFAULT_CHARSET
                EditLabel.Font.Color = clWindowText
                EditLabel.Font.Height = -13
                EditLabel.Font.Name = 'Tahoma'
                EditLabel.Font.Style = []
                EditLabel.ParentFont = False
                LabelPosition = lpLeft
                LabelSpacing = 5
                TabOrder = 0
                Text = '125'
                OnKeyDown = edlGradientKeyDown
              end
              object cxScrollBar1: TcxScrollBar
                Left = 199
                Top = 24
                Width = 250
                Height = 21
                Ctl3D = True
                LargeChange = 50
                LookAndFeel.Kind = lfOffice11
                LookAndFeel.NativeStyle = False
                LookAndFeel.SkinName = 'TheAsphaltWorld'
                Max = 65535
                PageSize = 0
                ParentCtl3D = False
                ParentShowHint = False
                Position = 125
                ShowHint = True
                SmallChange = 10
                UnlimitedTracking = True
                OnChange = cxScrollBar1Change
              end
              object edlBufferSize: TLabeledEdit
                Left = 104
                Top = 51
                Width = 89
                Height = 21
                Alignment = taRightJustify
                EditLabel.AlignWithMargins = True
                EditLabel.Width = 100
                EditLabel.Height = 16
                EditLabel.Margins.Right = 5
                EditLabel.Caption = #1056#1072#1079#1084#1077#1088' '#1073#1091#1092#1092#1077#1088#1072
                EditLabel.Font.Charset = DEFAULT_CHARSET
                EditLabel.Font.Color = clWindowText
                EditLabel.Font.Height = -13
                EditLabel.Font.Name = 'Tahoma'
                EditLabel.Font.Style = []
                EditLabel.ParentFont = False
                EditLabel.WordWrap = True
                LabelPosition = lpLeft
                LabelSpacing = 5
                TabOrder = 2
                Text = '125'
                OnKeyDown = edlBufferSizeKeyDown
              end
              object cxScrollBar2: TcxScrollBar
                Left = 199
                Top = 51
                Width = 250
                Height = 21
                Ctl3D = True
                LargeChange = 50
                LookAndFeel.Kind = lfOffice11
                LookAndFeel.NativeStyle = False
                LookAndFeel.SkinName = 'TheAsphaltWorld'
                Max = 255
                PageSize = 0
                ParentCtl3D = False
                ParentShowHint = False
                Position = 125
                ShowHint = True
                SmallChange = 10
                UnlimitedTracking = True
                OnChange = cxScrollBar2Change
              end
            end
            object TabSheet2: TTabSheet
              Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
              ImageIndex = 2
              TabVisible = False
              object grp1: TGroupBox
                Left = 0
                Top = 41
                Width = 452
                Height = 161
                Align = alTop
                Caption = #1050#1072#1085#1072#1083' 1'
                TabOrder = 0
                object Button7: TButton
                  Left = 327
                  Top = 27
                  Width = 81
                  Height = 25
                  Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
                  TabOrder = 0
                  OnClick = Button7Click
                end
                object edlDest1: TLabeledEdit
                  Left = 79
                  Top = 29
                  Width = 121
                  Height = 21
                  EditLabel.Width = 58
                  EditLabel.Height = 13
                  EditLabel.Caption = #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077
                  LabelPosition = lpLeft
                  TabOrder = 1
                end
                object edlSpeed1: TLabeledEdit
                  Left = 79
                  Top = 56
                  Width = 121
                  Height = 21
                  EditLabel.Width = 48
                  EditLabel.Height = 13
                  EditLabel.Caption = #1057#1082#1086#1088#1086#1089#1090#1100
                  LabelPosition = lpLeft
                  TabOrder = 2
                  Text = '1000'
                end
                object GroupBox2: TGroupBox
                  Left = 2
                  Top = 88
                  Width = 448
                  Height = 71
                  Align = alBottom
                  Caption = #1058#1077#1082#1091#1097#1077#1077' '#1087#1086#1083#1086#1078#1077#1085#1080#1077
                  Ctl3D = True
                  ParentCtl3D = False
                  TabOrder = 3
                  DesignSize = (
                    448
                    71)
                  object edlPosSteps1: TLabeledEdit
                    AlignWithMargins = True
                    Left = 15
                    Top = 39
                    Width = 121
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    EditLabel.Width = 47
                    EditLabel.Height = 13
                    EditLabel.Caption = #1055#1086' '#1096#1072#1075#1072#1084
                    TabOrder = 0
                    Text = '1'
                  end
                  object edlPosEnc1: TLabeledEdit
                    AlignWithMargins = True
                    Left = 161
                    Top = 39
                    Width = 125
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    Anchors = [akLeft, akTop, akRight]
                    EditLabel.Width = 64
                    EditLabel.Height = 13
                    EditLabel.Caption = #1055#1086' '#1101#1085#1082#1086#1076#1077#1088#1091
                    TabOrder = 1
                    Text = '1'
                  end
                  object edlPosReal1: TLabeledEdit
                    AlignWithMargins = True
                    Left = 312
                    Top = 39
                    Width = 121
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    Anchors = [akTop, akRight]
                    EditLabel.Width = 140
                    EditLabel.Height = 13
                    EditLabel.Caption = #1042' '#1075#1083#1086#1073#1072#1083#1100#1085#1099#1093' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072#1093
                    TabOrder = 2
                    Text = '1'
                  end
                end
                object btnReadPos: TButton
                  Left = 327
                  Top = 58
                  Width = 81
                  Height = 25
                  Caption = #1057#1095#1080#1090#1072#1090#1100
                  TabOrder = 4
                  OnClick = btnReadPosClick
                end
              end
              object GroupBox1: TGroupBox
                Left = 0
                Top = 202
                Width = 452
                Height = 162
                Align = alTop
                Caption = #1050#1072#1085#1072#1083' 2'
                TabOrder = 1
                object Button1: TButton
                  Left = 327
                  Top = 24
                  Width = 81
                  Height = 25
                  Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
                  TabOrder = 0
                  OnClick = Button1Click
                end
                object edlSpeed2: TLabeledEdit
                  Left = 79
                  Top = 64
                  Width = 121
                  Height = 21
                  EditLabel.Width = 48
                  EditLabel.Height = 13
                  EditLabel.Caption = #1057#1082#1086#1088#1086#1089#1090#1100
                  LabelPosition = lpLeft
                  TabOrder = 1
                  Text = '1000'
                end
                object edlDest2: TLabeledEdit
                  Left = 79
                  Top = 37
                  Width = 121
                  Height = 21
                  EditLabel.Width = 58
                  EditLabel.Height = 13
                  EditLabel.Caption = #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077
                  LabelPosition = lpLeft
                  TabOrder = 2
                end
                object GroupBox6: TGroupBox
                  Left = 2
                  Top = 89
                  Width = 448
                  Height = 71
                  Align = alBottom
                  Caption = #1058#1077#1082#1091#1097#1077#1077' '#1087#1086#1083#1086#1078#1077#1085#1080#1077
                  Ctl3D = True
                  ParentCtl3D = False
                  TabOrder = 3
                  DesignSize = (
                    448
                    71)
                  object edlPosSteps2: TLabeledEdit
                    AlignWithMargins = True
                    Left = 15
                    Top = 39
                    Width = 121
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    EditLabel.Width = 47
                    EditLabel.Height = 13
                    EditLabel.Caption = #1055#1086' '#1096#1072#1075#1072#1084
                    TabOrder = 0
                    Text = '1'
                  end
                  object edlPosEnc2: TLabeledEdit
                    AlignWithMargins = True
                    Left = 161
                    Top = 39
                    Width = 125
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    Anchors = [akLeft, akTop, akRight]
                    EditLabel.Width = 64
                    EditLabel.Height = 13
                    EditLabel.Caption = #1055#1086' '#1101#1085#1082#1086#1076#1077#1088#1091
                    TabOrder = 1
                    Text = '1'
                  end
                  object edlPosReal2: TLabeledEdit
                    AlignWithMargins = True
                    Left = 312
                    Top = 39
                    Width = 121
                    Height = 21
                    Margins.Left = 15
                    Margins.Right = 15
                    Alignment = taCenter
                    Anchors = [akTop, akRight]
                    EditLabel.Width = 140
                    EditLabel.Height = 13
                    EditLabel.Caption = #1042' '#1075#1083#1086#1073#1072#1083#1100#1085#1099#1093' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072#1093
                    TabOrder = 2
                    Text = '1'
                  end
                end
                object btnReadPos2: TButton
                  Left = 327
                  Top = 55
                  Width = 81
                  Height = 25
                  Caption = #1057#1095#1080#1090#1072#1090#1100
                  TabOrder = 4
                  OnClick = btnReadPos2Click
                end
              end
              object Panel3: TPanel
                Left = 0
                Top = 0
                Width = 452
                Height = 41
                Align = alTop
                Caption = 'Panel3'
                ShowCaption = False
                TabOrder = 2
                DesignSize = (
                  452
                  41)
                object btnConnectSmc: TcxButton
                  AlignWithMargins = True
                  Left = 1
                  Top = 5
                  Width = 80
                  Height = 30
                  Anchors = [akTop, akRight]
                  Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 0
                  WordWrap = True
                  OnClick = btnConnectSmcClick
                end
                object edlDivisor: TLabeledEdit
                  Left = 320
                  Top = 12
                  Width = 121
                  Height = 21
                  EditLabel.Width = 54
                  EditLabel.Height = 13
                  EditLabel.Caption = #1044#1077#1083#1080#1090#1077#1083#1100':'
                  LabelPosition = lpLeft
                  TabOrder = 1
                  Text = '1'
                  OnKeyDown = edlDivisorKeyDown
                end
                object btnStart: TcxButton
                  AlignWithMargins = True
                  Left = 87
                  Top = 5
                  Width = 80
                  Height = 30
                  Anchors = [akTop, akRight]
                  Caption = #1047#1072#1087#1091#1089#1082' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 2
                  WordWrap = True
                  OnClick = btnStartClick
                end
                object btnStop: TcxButton
                  AlignWithMargins = True
                  Left = 173
                  Top = 5
                  Width = 80
                  Height = 30
                  Anchors = [akTop, akRight]
                  Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1080#1079#1084#1077#1088#1077#1085#1080#1077
                  LookAndFeel.NativeStyle = False
                  LookAndFeel.SkinName = 'TheAsphaltWorld'
                  TabOrder = 3
                  WordWrap = True
                  OnClick = btnStopClick
                end
              end
              object GroupBox7: TGroupBox
                Left = 329
                Top = 364
                Width = 123
                Height = 286
                Align = alClient
                Caption = #1057#1077#1090#1082#1072
                TabOrder = 3
                object Memo1: TMemo
                  Left = 2
                  Top = 15
                  Width = 119
                  Height = 269
                  Align = alClient
                  TabOrder = 0
                end
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 364
                Width = 329
                Height = 286
                Align = alLeft
                Caption = #1044#1072#1085#1085#1099#1077' '#1080#1079#1084#1077#1088#1077#1085#1080#1081
                TabOrder = 4
                object sgPSD: TStringGrid
                  Left = 2
                  Top = 15
                  Width = 325
                  Height = 269
                  Align = alClient
                  ColCount = 4
                  FixedCols = 0
                  RowCount = 2
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = #1056#1077#1078#1080#1084' '#1074#1080#1076#1077#1086
        ImageIndex = 1
        TabVisible = False
        object spl2: TSplitter
          Left = 802
          Top = 0
          Height = 680
          Align = alRight
          ExplicitLeft = 808
          ExplicitHeight = 722
        end
        object Panel5: TPanel
          Left = 805
          Top = 0
          Width = 449
          Height = 680
          Align = alRight
          TabOrder = 0
          object RadioGroup1: TRadioGroup
            AlignWithMargins = True
            Left = 4
            Top = 4
            Width = 441
            Height = 105
            Align = alTop
            Caption = #1056#1077#1078#1080#1084
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Items.Strings = (
              #1043#1080#1089#1090#1086#1075#1088#1072#1084#1084#1072
              #1058#1086#1095#1082#1080' '#1075#1088#1072#1076#1080#1077#1085#1090#1072
              #1040#1087#1087#1088#1086#1082#1089#1080#1084#1072#1094#1080#1103)
            ParentFont = False
            TabOrder = 0
            OnClick = RadioGroup1Click
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 802
          Height = 680
          Align = alClient
          TabOrder = 1
          object Image1: TImage
            Left = 1
            Top = 1
            Width = 800
            Height = 678
            Align = alClient
            ExplicitWidth = 783
          end
        end
      end
    end
  end
  object mm1: TMainMenu
    Left = 24
    Top = 32
    object N1: TMenuItem
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1074#1103#1079#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1080#1076
      OnClick = N2Click
      object N5: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1087#1086#1084#1086#1075#1072#1090#1077#1083#1100#1085#1099#1077' '#1088#1072#1089#1095#1077#1090#1099
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1103' '#1074#1089#1087#1086#1084#1086#1075#1072#1090#1077#1083#1100#1085#1099#1093' '#1088#1072#1089#1095#1077#1090#1086#1074
        OnClick = N6Click
      end
      object N8: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1075#1088#1072#1085#1080#1094#1099' '#1088#1077#1075#1080#1086#1085#1086#1074
        Checked = True
        OnClick = N8Click
      end
      object N9: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1072#1087#1087#1088#1086#1082#1089#1080#1084#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1092#1080#1075#1091#1088#1099
        Checked = True
        OnClick = N9Click
      end
      object N7: TMenuItem
        Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1081' '#1088#1077#1075#1080#1086#1085
        OnClick = N7Click
      end
      object N10: TMenuItem
        Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1091#1102' '#1092#1080#1075#1091#1088#1091
        OnClick = N10Click
      end
    end
    object N11: TMenuItem
      Caption = #1057#1077#1088#1074#1080#1089
      object N12: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1076#1072#1090#1095#1080#1082#1072
        Visible = False
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1090#1077#1088#1084#1080#1085#1072#1083
        OnClick = N13Click
      end
      object N14: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1085#1090#1088#1072#1089#1090#1072
        OnClick = N14Click
      end
    end
    object N3: TMenuItem
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100' '#1082#1072#1076#1088
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1048#1084#1087#1086#1088#1090' '#1082#1072#1076#1088#1072'...'
      OnClick = N4Click
    end
  end
  object FileOpenDialog1: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = #1060#1072#1081#1083#1099' '#1087#1088#1086#1092#1080#1083#1103' '#1076#1077#1090#1072#1083#1080
        FileMask = '*.txt'
      end>
    Options = []
    Left = 20
    Top = 84
  end
  object TeeGDIPlus1: TTeeGDIPlus
    Active = True
    Left = 16
    Top = 144
  end
end
