//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormContrast.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsCore"
#pragma link "dxSkinTheAsphaltWorld"
#pragma resource "*.dfm"
TFormCont *FormCont;
//---------------------------------------------------------------------------
__fastcall TFormCont::TFormCont(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormCont::btnWriteClick(TObject *Sender)
{
	String sRequest;
	String sTemp;
	sRequest = 	FormTerm->PrepareString(edlAddr->Text,4) +
				FormTerm->PrepareString(edlMask->Text,4) +
				FormTerm->PrepareString(edlValue->Text,4);
	thrDataStream->Write("setcont"+sRequest);
}
//---------------------------------------------------------------------------
void __fastcall TFormCont::btnReadClick(TObject *Sender)
{
	String sResponse;
	sResponse = thrDataStream->Read("getcont");
	edlValue->Text = sResponse.SubString(8,4);
}
//---------------------------------------------------------------------------
