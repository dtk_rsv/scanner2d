//---------------------------------------------------------------------------

#ifndef VertexH
#define VertexH
#include "Point.h"
#include "Node.h"
//---------------------------------------------------------------------------
namespace Geometry
{
	enum {CLOCKWISE,COUNTER_CLOCKWISE};
	class Vertex: public Node, public  Point
	{
	 public:

	  Vertex (double x, double y);
	  Vertex (Point&);
	  Vertex *cw(void);
	  Vertex *ccw(void);
	  Vertex *neighbor (int rotation);
	  Point point (void);
	  Vertex *insert (Vertex* );
	  Vertex *remove (void);
	  void splice (Vertex*);
	  Vertex *split (Vertex*);
	  friend class Polygon;

	};
}
#endif
