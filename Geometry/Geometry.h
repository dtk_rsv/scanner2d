//---------------------------------------------------------------------------

#ifndef GeometryH
#define GeometryH
#include "Point.h"
#include "Edge.h"
#include "Node.h"
#include "Polygon.h"
#include "Vertex.h"
//---------------------------------------------------------------------------
namespace Geometry
{
	class Edge;
	class Node;
	class Point;
	class Polygon;
	class Vertex;
}

#endif
