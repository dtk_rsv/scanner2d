#ifndef PointH
#define PointH
#include <math.h>	//sqrt

namespace Geometry
{

	enum {LEFT,  RIGHT,  BEYOND,  BEHIND, BETWEEN, ORIGIN, DESTINATION};
	class Edge;
	class Point
	{
		public:

			Point() : x(0.0), y(0.0) {}
			Point(double x_coord, double y_coord) : x(x_coord), y(y_coord) {}
			void SetX(double value) { x = value; }
			void SetY(double value) { y = value; }
			double GetX() const { return x; }
			double GetY() const { return y; }
			void Reset() { x = 0.0; y = 0.0; }
			double x;
			double y;

			Point operator + (Point&);
			Point operator - (Point&);
			Point friend operator * (double,Point&);

			double operator[] (int);

			bool operator == (Point&);
			bool operator != (Point&);

			bool operator < (Point&);
			bool operator > (Point&);

			int classify(Point&, Point&);

			double Length();
			double polarAngle(void);

			double length(void);

	};
}

#endif
