//---------------------------------------------------------------------------

#ifndef PolygonH
#define PolygonH
//#include "Geometry.h"
#include "Vertex.h"
#include "Edge.h"
//---------------------------------------------------------------------------
namespace Geometry
{
	class Vertex;
	class Point;
	class Edge;
	class Polygon
	{
		private:
			Vertex *_v;
			int _size;
			void resize (void);
		public:
			Polygon (void);
			Polygon (Polygon&);
			Polygon (Vertex*);
			~Polygon (void);
			Vertex *v(void);
			int size (void);
			Point point (void);
			Edge edge (void);
			Vertex *cw(void);
			Vertex *ccw (void);
			Vertex *neighbor (int rotation);
			Vertex *advance (int rotation);
			Vertex *setV (Vertex*);
			Vertex *insert (Point&);
			void remove (void);
			Polygon * split (Vertex*);
			bool pointlnConvexPolygon (Point &s , Polygon &p);
	};
}
#endif
