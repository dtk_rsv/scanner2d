#pragma hdrstop
#include "Point.h"
//#include "Edge.h"
#pragma package(smart_init)
namespace Geometry
{
	Point Point::operator + (Point &p)
	{
		return Point (x + p.x, y + p.y);
	}

	Point Point::operator - (Point &p)
	{
		return Point (x - p.x, y - p.y);
	}

	Point operator * (double s , Point &p)
	{
		return Point (s * p.x, s * p.y);
	}

	double Point::operator [] (int i)
	{
	  return (i == 0) ? x : y;
	}

	bool Point::operator == (Point &p)
	{
		return (x == p.x) && (y == p.y);
	}

	bool Point::operator != (Point &p)
	{
		return !(*this == p);
	}

	bool Point::operator < (Point &p)
	{
		return ((x < p.x) || ((x == p.x) && (y<p.y)));
	}

	bool Point::operator > (Point &p)
	{
		return ((x>p.x) || ((x == p.x) && (y > p.y)));
	}

	int Point::classify(Point &p0, Point &p1)
	{
		Point p2 = *this;
		Point a = p1 - p0;
		Point b = p2 - p0;
		double sa = a. x * b.y - b.x * a.y;
		if (sa > 0.0)
			return LEFT;
		if (sa < 0.0)
			return RIGHT;
		if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
			return BEHIND;
		if (a.Length() < b.Length())
			return BEYOND;
		if (p0 == p2)
			return ORIGIN;
		if (p1 == p2)
			return DESTINATION;
		 return BETWEEN;
	}

	double Point::polarAngle(void)
	{
	  if ((x == 0.0) && (y == 0.0))
		return -1.0;
	  if (x == 0.0)
		return ((y > 0.0) ? 90 : 270);
	  double theta = atan(y/x);                    // � ��������
	  theta *= 360 / (2 * 3.1415926);            // ������� � �������
	  if (x > 0.0)                                 // 1 � 4 ���������
		return ((y >= 0.0) ? theta : 360 + theta);
	  else                                         // 2 � � ���������
		return (180 + theta);
	}
	double Point::Length (void)
	{
	  return sqrt(x*x + y*y);
	}

}
