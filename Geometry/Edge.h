//---------------------------------------------------------------------------

#ifndef EdgeH
#define EdgeH
//---------------------------------------------------------------------------
//#include "Geometry.h"
#include "Point.h"
namespace Geometry
{

	enum { COLLINEAR, PARALLEL, SKEW, SKEW_CROSS, SKEW_NO_CROSS };
	class Point;
	class Edge
	{
	 public:

	  Point org;
	  Point dest;
	  Edge (Point & _org, Point & _dest);
	  Edge (void);
	  Edge &rot (void);
	  Edge &flip (void);
	  int classify(Point &);
	  Point point (double);
	  int intersect (Edge&, double&);
	  int cross (Edge&, double&);
	  bool isVertical(void);
	  double slope(void);
	  double �(double);
	  double distance(Point & p);
	};

}

#endif
