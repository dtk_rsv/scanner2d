//---------------------------------------------------------------------------

#ifndef FormTerminalH
#define FormTerminalH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinTheAsphaltWorld.hpp"
#include <Vcl.Menus.hpp>
#include "main.h"
//---------------------------------------------------------------------------
class TFormTerm : public TForm
{
__published:	// IDE-managed Components
	TRichEdit *redtTerminal;
	TPanel *Panel1;
	TLabeledEdit *edlAddr;
	TLabeledEdit *edlMask;
	TLabeledEdit *edlValue;
	TcxButton *btnWrite;
	TcxButton *btnRead;
	void __fastcall btnWriteClick(TObject *Sender);
	void __fastcall btnReadClick(TObject *Sender);
private:	// User declarations

public:		// User declarations
	__fastcall TFormTerm(TComponent* Owner);
	String PrepareString(String,int Len);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormTerm *FormTerm;
//---------------------------------------------------------------------------
#endif
