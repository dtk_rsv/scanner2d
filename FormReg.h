//---------------------------------------------------------------------------

#ifndef FormRegH
#define FormRegH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "AdvGrid.hpp"
#include "AdvObj.hpp"
#include "BaseGrid.hpp"
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinTheAsphaltWorld.hpp"
#include <Vcl.Grids.hpp>
#include <Vcl.Menus.hpp>

#include "NetCommands.h"
#include <vector>
//---------------------------------------------------------------------------
class TFormSettings : public TForm
{
__published:	// IDE-managed Components
	TcxButton *btnAddShape;
	TcxButton *cxButton1;
	TAdvStringGrid *sgRegMap;
	TcxButton *cxButton2;
	TcxButton *cxButton3;
	void __fastcall sgRegMapMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall btnAddShapeClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
  	std::vector<NetCmd> _commands;
public:		// User declarations
	__fastcall TFormSettings(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSettings *FormSettings;
//---------------------------------------------------------------------------
#endif
