object FormCont: TFormCont
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1085#1090#1088#1072#1089#1090#1072
  ClientHeight = 85
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = -1
    Width = 394
    Height = 86
    Align = alBottom
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    ExplicitTop = 214
    ExplicitWidth = 391
    DesignSize = (
      394
      86)
    object edlAddr: TLabeledEdit
      Left = 8
      Top = 32
      Width = 121
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 53
      EditLabel.Height = 16
      EditLabel.Caption = #1052#1080#1085#1080#1084#1091#1084
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 0
    end
    object edlMask: TLabeledEdit
      Left = 135
      Top = 31
      Width = 124
      Height = 21
      Alignment = taRightJustify
      Anchors = [akLeft, akTop, akRight]
      EditLabel.Width = 58
      EditLabel.Height = 16
      EditLabel.Caption = #1052#1072#1082#1089#1080#1084#1091#1084
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 1
      ExplicitWidth = 121
    end
    object edlValue: TLabeledEdit
      Left = 265
      Top = 31
      Width = 121
      Height = 21
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      EditLabel.Width = 34
      EditLabel.Height = 16
      EditLabel.Caption = #1055#1086#1088#1086#1075
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 2
      ExplicitLeft = 262
    end
    object btnWrite: TcxButton
      AlignWithMargins = True
      Left = 80
      Top = 58
      Width = 111
      Height = 25
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'TheAsphaltWorld'
      TabOrder = 3
      OnClick = btnWriteClick
    end
    object btnRead: TcxButton
      AlignWithMargins = True
      Left = 200
      Top = 58
      Width = 111
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1057#1095#1080#1090#1072#1090#1100
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'TheAsphaltWorld'
      TabOrder = 4
      OnClick = btnReadClick
      ExplicitLeft = 197
    end
  end
end
