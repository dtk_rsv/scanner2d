object FormTerm: TFormTerm
  Left = 0
  Top = 0
  Caption = #1058#1077#1088#1084#1080#1085#1072#1083
  ClientHeight = 534
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object redtTerminal: TRichEdit
    Left = 0
    Top = 0
    Width = 391
    Height = 448
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    Zoom = 100
  end
  object Panel1: TPanel
    Left = 0
    Top = 448
    Width = 391
    Height = 86
    Align = alBottom
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    DesignSize = (
      391
      86)
    object edlAddr: TLabeledEdit
      Left = 8
      Top = 32
      Width = 121
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 91
      EditLabel.Height = 16
      EditLabel.Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 0
    end
    object edlMask: TLabeledEdit
      Left = 135
      Top = 31
      Width = 121
      Height = 21
      Alignment = taRightJustify
      Anchors = [akLeft, akTop, akRight]
      EditLabel.Width = 36
      EditLabel.Height = 16
      EditLabel.Caption = #1052#1072#1089#1082#1072
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 1
    end
    object edlValue: TLabeledEdit
      Left = 262
      Top = 31
      Width = 121
      Height = 21
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      EditLabel.Width = 56
      EditLabel.Height = 16
      EditLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -13
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      TabOrder = 2
    end
    object btnWrite: TcxButton
      AlignWithMargins = True
      Left = 80
      Top = 58
      Width = 111
      Height = 25
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'TheAsphaltWorld'
      TabOrder = 3
      OnClick = btnWriteClick
    end
    object btnRead: TcxButton
      AlignWithMargins = True
      Left = 197
      Top = 58
      Width = 111
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1057#1095#1080#1090#1072#1090#1100
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'TheAsphaltWorld'
      TabOrder = 4
      OnClick = btnReadClick
    end
  end
end
