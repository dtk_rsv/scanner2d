var class_multi_region =
[
    [ "MultiRegion", "d1/dd6/class_multi_region.html#a8a410827f3c036bd0b7d337d9f13f464", null ],
    [ "~MultiRegion", "d1/dd6/class_multi_region.html#a57e2813a4013a02778442e4d9bd017b5", null ],
    [ "GetPointsInside", "d1/dd6/class_multi_region.html#a140cb72b7a64b3e38c92a8e0a966f4ca", null ],
    [ "GetPointsInside", "d1/dd6/class_multi_region.html#a962bb829df4892c2583dc2c9ad590fe5", null ],
    [ "GetRegionBoundariesX", "d1/dd6/class_multi_region.html#a30cbfdc7897b4c7e5be39e328dfcdc92", null ],
    [ "Hide", "d1/dd6/class_multi_region.html#a4c265cdc22c6f78b152e154580ead6fa", null ],
    [ "KeyDown", "d1/dd6/class_multi_region.html#aabe839482bf7dd75626442ad972f614d", null ],
    [ "MakeSelection", "d1/dd6/class_multi_region.html#af1745b9370df5761b47871574f1aff34", null ],
    [ "MouseDown", "d1/dd6/class_multi_region.html#aaa83d97f6bdbe27e30b8c2fb66f2c5b2", null ],
    [ "MouseMove", "d1/dd6/class_multi_region.html#a26bfa91261e7cc888adc1abd78b848bf", null ],
    [ "RemoveSelection", "d1/dd6/class_multi_region.html#a565c6aa303cbd9c8d711c64ef11f2489", null ],
    [ "SetVertex", "d1/dd6/class_multi_region.html#a7e23363ad6c78d46a54b25d99cbc890c", null ],
    [ "Show", "d1/dd6/class_multi_region.html#af50a5017fea4a006e5f86929dd14f18f", null ]
];