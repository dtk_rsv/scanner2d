var class_raw_data =
[
    [ "RawData", "d1/d3a/class_raw_data.html#a583cd50c06a9fbaebdaaf44621401552", null ],
    [ "Assign", "d1/d3a/class_raw_data.html#ad6d19f4b4615bd5dce171567b188aba4", null ],
    [ "Clear", "d1/d3a/class_raw_data.html#a561f4df963e685ad2a419166ff57ad17", null ],
    [ "GetPoint", "d1/d3a/class_raw_data.html#a4db7073cd5a8a27f3cd36225c4062255", null ],
    [ "GetSize", "d1/d3a/class_raw_data.html#a78ed05054553be55e28cc239aae91a27", null ],
    [ "GetStatus", "d1/d3a/class_raw_data.html#af2a572845607cd9b9b8656bfdee530b0", null ],
    [ "ReadFromFile", "d1/d3a/class_raw_data.html#a8e79f9f66609a081b23052c745fe2f4d", null ],
    [ "SetPoint", "d1/d3a/class_raw_data.html#a7f4c699f27c8bfcdb3d90b15063d432b", null ]
];