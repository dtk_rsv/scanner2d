var class_region =
[
    [ "FRAME", "class_region.html#a67cb752c0bb543632f921fac74efe95ca51e1c8ff13b89c38327fdbd75f0203d9", null ],
    [ "TRIANGLE", "class_region.html#a67cb752c0bb543632f921fac74efe95caf96e320d57ab7dddea995d29315c6b0d", null ],
    [ "FREEFORM", "class_region.html#a67cb752c0bb543632f921fac74efe95cafcdb19f3568f5b19a067bf9512eeed04", null ],
    [ "MULTIREGION", "class_region.html#a67cb752c0bb543632f921fac74efe95cafe2a8d03a6f44c715917fd01886a73c2", null ],
    [ "Region", "class_region.html#a5637c5dd01582f3ad968fcd055652db6", null ],
    [ "~Region", "class_region.html#a3c3670fff78f7511d156e3b2f0bc6266", null ],
    [ "GetDataBoundariesX", "class_region.html#a0e8f53b1de1396cc8056832e3bb10154", null ],
    [ "GetName", "class_region.html#abe6c4e51c83ff458b146a2aa27287733", null ],
    [ "GetPointsInside", "class_region.html#a22b4e248b4d0e8f7071ec536ad2ac51a", null ],
    [ "GetPointsInside", "class_region.html#af0099aceff3554e0f79793526c9e4e53", null ],
    [ "GetRegionBoundariesX", "class_region.html#ae4616b54d730eb62fc94c5ee2c6f357b", null ],
    [ "GetType", "class_region.html#aa7efc9fbf8ae93e9b343b0700cdbc78a", null ],
    [ "MakeSelection", "class_region.html#aadb1cc7700aaebecae5c5a50addda615", null ],
    [ "MouseDown", "class_region.html#a6d951f2174a9ccef26abb6566577991c", null ],
    [ "MouseMove", "class_region.html#a872504f166324ac0104a6d1a21722932", null ],
    [ "RemoveSelection", "class_region.html#a5e92853a138bee44dd9586472e924ad2", null ],
    [ "SetPoints", "class_region.html#a7a5823fa599509d6d61ba439cbb0fc79", null ],
    [ "SetVertex", "class_region.html#aa73f04930c7dfab81481b501a03d095f", null ],
    [ "count", "class_region.html#a07fbb7bc87c40908c9c14f885e932d84", null ],
    [ "data", "class_region.html#a194ec9b5552c4e1f404db950c9c0ef3b", null ],
    [ "graph", "class_region.html#a1c2a60ae277eee0b99ce1d025ee3a41f", null ]
];