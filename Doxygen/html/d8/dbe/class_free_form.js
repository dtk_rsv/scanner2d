var class_free_form =
[
    [ "FreeForm", "d8/dbe/class_free_form.html#af01f8dc2bd7d788bddad83fab8ae7b59", null ],
    [ "~FreeForm", "d8/dbe/class_free_form.html#a3cabdf0bb8a56debe8a0cbd5e9a80c1a", null ],
    [ "_dabs", "d8/dbe/class_free_form.html#ae29413f316f02751a950cbb0be5638ff", null ],
    [ "GetPointsInside", "d8/dbe/class_free_form.html#a4be4293183914bd09fdbe03fdd6a7dfd", null ],
    [ "GetPointsInside", "d8/dbe/class_free_form.html#afb473724cd74964dc46e4ff2824a51ef", null ],
    [ "GetRegionBoundariesX", "d8/dbe/class_free_form.html#a3b27fd734b71e64661fe5c5839dc9ae1", null ],
    [ "Hide", "d8/dbe/class_free_form.html#abbe62c172439670993b1aad96c95f0b0", null ],
    [ "KeyDown", "d8/dbe/class_free_form.html#abf5af4e28cd2d0fc8fd275ac0160269b", null ],
    [ "MakeSelection", "d8/dbe/class_free_form.html#af6dc1bb8bd7a881e58a547ea7d750d46", null ],
    [ "MouseDown", "d8/dbe/class_free_form.html#a75edb9754a2c97a9967b9d8d527c6830", null ],
    [ "MouseMove", "d8/dbe/class_free_form.html#ab94bac07afef8ad59bde37fe523c23f1", null ],
    [ "RemoveSelection", "d8/dbe/class_free_form.html#ab5c75eb13ab076d5e5cbe6a8faa20695", null ],
    [ "SetVertex", "d8/dbe/class_free_form.html#acc52d11f9fc0235ea397c8dd2149465a", null ],
    [ "Show", "d8/dbe/class_free_form.html#a739424f32ff1775855c1e397d2b06802", null ]
];