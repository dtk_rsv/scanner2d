var class_point2_d =
[
    [ "Point2D", "class_point2_d.html#a2415006d697f1c222c17254bdd302098", null ],
    [ "Point2D", "class_point2_d.html#abe6e081e0b06043a9992c17d8fef0dd7", null ],
    [ "GetX", "class_point2_d.html#a4fd8df97d520e34be15e5a98a2554bbb", null ],
    [ "GetY", "class_point2_d.html#a8843ebf5a9077c3f8fb905286b12a244", null ],
    [ "Reset", "class_point2_d.html#abb2d709aeb63849d2efd16d26155736b", null ],
    [ "SetX", "class_point2_d.html#abdb0202e3f987fa830f61bd89274744d", null ],
    [ "SetY", "class_point2_d.html#af8547d31eb42cff6891db6a93b550efe", null ]
];