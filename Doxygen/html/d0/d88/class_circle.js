var class_circle =
[
    [ "Circle", "d0/d88/class_circle.html#adf204fdba1fc6fcaab454f0e88457eb8", null ],
    [ "~Circle", "d0/d88/class_circle.html#ae3f30436e645d73e368e8ee55f8d1650", null ],
    [ "Calculate", "d0/d88/class_circle.html#a135c9562dcce282b3a7d9c76034d48ce", null ],
    [ "GetResults", "d0/d88/class_circle.html#ab365930f4252292c5b4f73792b9ec87f", null ],
    [ "Hide", "d0/d88/class_circle.html#a2259f32cc0a3ab0259fbf742d4d544f3", null ],
    [ "MakeSelection", "d0/d88/class_circle.html#a1bcc146575905b86207581d6ccf29088", null ],
    [ "RemoveSelection", "d0/d88/class_circle.html#a069e395620c9dfe0a81c6d9a83b66fad", null ],
    [ "Show", "d0/d88/class_circle.html#a7eaf04146b79920c75d88089c44b44f2", null ]
];