var hierarchy =
[
    [ "Calculation", "d6/d9a/class_calculation.html", [
      [ "AngleBetweenLines", "d9/d59/class_angle_between_lines.html", null ],
      [ "DistanceBetweenCenters", "df/d16/class_distance_between_centers.html", null ],
      [ "DistanceBetweenLines", "dd/d22/class_distance_between_lines.html", null ]
    ] ],
    [ "Calibration", "d2/d40/class_calibration.html", null ],
    [ "Geometry::Edge", "dd/d8b/class_geometry_1_1_edge.html", null ],
    [ "Filter", "d2/d70/class_filter.html", null ],
    [ "Filter2D", "d6/dac/class_filter2_d.html", null ],
    [ "Graph", "da/d9a/class_graph.html", null ],
    [ "GraphObserver", "de/d04/class_graph_observer.html", [
      [ "TableCalculation", "da/da8/class_table_calculation.html", null ],
      [ "TableRegion", "d5/db2/class_table_region.html", null ],
      [ "TableShape", "d7/de1/class_table_shape.html", null ]
    ] ],
    [ "Geometry::Node", "d5/d67/class_geometry_1_1_node.html", [
      [ "Geometry::Vertex", "db/d17/class_geometry_1_1_vertex.html", null ]
    ] ],
    [ "Geometry::Point", "d1/d4b/class_geometry_1_1_point.html", [
      [ "Geometry::Vertex", "db/d17/class_geometry_1_1_vertex.html", null ]
    ] ],
    [ "Point", "d0/d69/class_point.html", null ],
    [ "Point2D", "d1/d1d/class_point2_d.html", null ],
    [ "Geometry::Polygon", "da/d9a/class_geometry_1_1_polygon.html", null ],
    [ "RawData", "d1/d3a/class_raw_data.html", null ],
    [ "Region", "df/d6c/class_region.html", [
      [ "Frame", "d5/d19/class_frame.html", null ],
      [ "FreeForm", "d8/dbe/class_free_form.html", null ],
      [ "MultiRegion", "d1/dd6/class_multi_region.html", null ],
      [ "Triangle", "d2/d28/class_triangle.html", null ]
    ] ],
    [ "Shape", "d7/da7/class_shape.html", [
      [ "TCircle", "df/d74/class_t_circle.html", null ],
      [ "TEllipse", "d7/d07/class_t_ellipse.html", null ],
      [ "TLine", "d0/da0/class_t_line.html", null ]
    ] ],
    [ "sMeasureParams", "d7/df8/structs_measure_params.html", null ],
    [ "TForm", null, [
      [ "TFormMain", "d5/d07/class_t_form_main.html", null ]
    ] ],
    [ "TMeasurement", "d1/d06/class_t_measurement.html", null ],
    [ "TThread", null, [
      [ "Log", "d6/d70/class_log.html", null ],
      [ "TDataStream", "df/db7/class_t_data_stream.html", null ]
    ] ]
];