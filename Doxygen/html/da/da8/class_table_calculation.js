var class_table_calculation =
[
    [ "TableCalculation", "da/da8/class_table_calculation.html#a67499cb72b13aa5c3b46d6b55979e723", null ],
    [ "~TableCalculation", "da/da8/class_table_calculation.html#a5c5b0e6abc53672d560a9b3dead28f62", null ],
    [ "first", "da/da8/class_table_calculation.html#aa849e04cc3d97c02b7b747ad582211bc", null ],
    [ "GetAll", "da/da8/class_table_calculation.html#a91bf5383d2ab99c63bd6ab69679c7d08", null ],
    [ "GetCurrentCalculation", "da/da8/class_table_calculation.html#a96bc286a6e47eecae97d74e453aa6d1c", null ],
    [ "GetSelected", "da/da8/class_table_calculation.html#a15a565098670cfd96e1d502bd3cda542", null ],
    [ "HideAux", "da/da8/class_table_calculation.html#a2ad9a731360cc0ed7de434b1408842d0", null ],
    [ "HideMarks", "da/da8/class_table_calculation.html#a345c6777a5a9b2f897aca402565e02fd", null ],
    [ "last", "da/da8/class_table_calculation.html#aa2b2b766cf46e2aa206805f420bee56d", null ],
    [ "next", "da/da8/class_table_calculation.html#a9adc7bef315a9c1dba84532083b6f220", null ],
    [ "ResetAll", "da/da8/class_table_calculation.html#acf52d10e852d00ff4725f2925fbc8d41", null ],
    [ "SetSelectStatus", "da/da8/class_table_calculation.html#a315ad4b37ecd77c0fd67abfa0b3b5d1e", null ],
    [ "ShowAux", "da/da8/class_table_calculation.html#a752e0051d64916f28d5545bc31b23502", null ],
    [ "ShowMarks", "da/da8/class_table_calculation.html#a267f60f447fe9a15cc4ac6c8847878e9", null ],
    [ "update", "da/da8/class_table_calculation.html#a58bbd44c37e9ff4416c0d37d9901687e", null ],
    [ "update", "da/da8/class_table_calculation.html#adfdba3a0bb8a4c11fcda15a8e1d13656", null ],
    [ "update", "da/da8/class_table_calculation.html#a41a11a7bc42c489f6d3c73a48f38e567", null ]
];