var class_graph =
[
    [ "Graph", "class_graph.html#a45925320a320219c1aedcb67d9b6c50b", null ],
    [ "AddData", "class_graph.html#a27f3a5d483f6556217e66243ba97a66a", null ],
    [ "AddRegion", "class_graph.html#a1c0a919b15f9973a5dc8cb2c84c270ca", null ],
    [ "AddShape", "class_graph.html#a9db7ca0b22e148600cf21a18f5e303f2", null ],
    [ "attach", "class_graph.html#a28e124c51379c1eaf59cc63c5a180cd9", null ],
    [ "DeleteActiveRegion", "class_graph.html#a8e78900480ee6d0a070f11a46cf43c2d", null ],
    [ "DeleteAllRegions", "class_graph.html#a977d3ec086a9b5c0f7481eee3a2da00d", null ],
    [ "DeleteRegion", "class_graph.html#a338ab74128d867bec9fd7b3f6a1f9418", null ],
    [ "DeleteShape", "class_graph.html#ad2732b4987eb7d1ea5b117ecf619f960", null ],
    [ "detach", "class_graph.html#a631d74a535f8a1f4bacf97cf209b0153", null ],
    [ "GetContext", "class_graph.html#a8c9d1cb46a77d2bde6accdc604f36c15", null ],
    [ "GetMode", "class_graph.html#aad3d279b9f520283e754038406372bb7", null ],
    [ "GraphMouseDown", "class_graph.html#a8f8ac1144a6ab85f2b8a6a919334d4b3", null ],
    [ "GraphMouseMove", "class_graph.html#aadae04a6cb0909ae0f127e0aaec5a815", null ],
    [ "GraphMouseUp", "class_graph.html#a10a4d08e794b386e56186428b50e7b01", null ],
    [ "NewRegionCreated", "class_graph.html#a903aee26f8923c11b6b685768edc806d", null ],
    [ "NewShapeCreated", "class_graph.html#a280b1254e5d0797bb70bb457f757c58c", null ],
    [ "notify", "class_graph.html#a694228193c12b15adc2643d66bb9021a", null ],
    [ "notify", "class_graph.html#af7c4393fecdad924e10d0b68613616b3", null ],
    [ "SetActiveRegion", "class_graph.html#a358b35f4b822b3055a1293dc753db74c", null ],
    [ "SetMode", "class_graph.html#a04dc1912abb3c6c90ae480c69f9f4991", null ]
];