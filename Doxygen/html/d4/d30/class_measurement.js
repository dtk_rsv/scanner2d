var class_measurement =
[
    [ "ANGLE_BETWEEN_LINES", "d4/d30/class_measurement.html#a551aab3f4916255c111a4ac14a38d240affa40bfdaf5d6ac6191354afad88303c", null ],
    [ "DISTANCE_BETWEEN_CENTERS_CIRCLES", "d4/d30/class_measurement.html#a551aab3f4916255c111a4ac14a38d240a691137a916e3e46be55f155258cec0bf", null ],
    [ "Measurement", "d4/d30/class_measurement.html#a13d886013e294fb740422d8b64e8af14", null ],
    [ "Calculate", "d4/d30/class_measurement.html#a625ed07fa5a1ac2bc17e5a804df994c1", null ],
    [ "GetResult", "d4/d30/class_measurement.html#ae24f7e550ec033cffeb4b7d249422bcd", null ],
    [ "GetSize", "d4/d30/class_measurement.html#a27fdd3396806854015f4e4e71416939e", null ],
    [ "result", "d4/d30/class_measurement.html#aa4ef5304ec2bb57600d9beb738557b8e", null ],
    [ "shapes", "d4/d30/class_measurement.html#a2cf0b7b38e827e69a2d1778e182cddf1", null ]
];