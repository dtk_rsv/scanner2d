var class_circle =
[
    [ "Circle", "class_circle.html#adf204fdba1fc6fcaab454f0e88457eb8", null ],
    [ "~Circle", "class_circle.html#ae3f30436e645d73e368e8ee55f8d1650", null ],
    [ "Calculate", "class_circle.html#a135c9562dcce282b3a7d9c76034d48ce", null ],
    [ "GetResults", "class_circle.html#ab365930f4252292c5b4f73792b9ec87f", null ],
    [ "Hide", "class_circle.html#a2259f32cc0a3ab0259fbf742d4d544f3", null ],
    [ "MakeSelection", "class_circle.html#a1bcc146575905b86207581d6ccf29088", null ],
    [ "RemoveSelection", "class_circle.html#a069e395620c9dfe0a81c6d9a83b66fad", null ],
    [ "Show", "class_circle.html#a7eaf04146b79920c75d88089c44b44f2", null ]
];