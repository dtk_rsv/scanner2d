var class_multi_region =
[
    [ "MultiRegion", "class_multi_region.html#aa08b4b89bbbeb0c32193b10bbff6cee9", null ],
    [ "~MultiRegion", "class_multi_region.html#a57e2813a4013a02778442e4d9bd017b5", null ],
    [ "GetPointsInside", "class_multi_region.html#a140cb72b7a64b3e38c92a8e0a966f4ca", null ],
    [ "GetPointsInside", "class_multi_region.html#a962bb829df4892c2583dc2c9ad590fe5", null ],
    [ "GetRegionBoundariesX", "class_multi_region.html#a30cbfdc7897b4c7e5be39e328dfcdc92", null ],
    [ "MakeSelection", "class_multi_region.html#a4972eda391f3871d804b26a7517b56dc", null ],
    [ "MouseDown", "class_multi_region.html#aaa83d97f6bdbe27e30b8c2fb66f2c5b2", null ],
    [ "MouseMove", "class_multi_region.html#a26bfa91261e7cc888adc1abd78b848bf", null ],
    [ "RemoveSelection", "class_multi_region.html#a3453977152a56ba75b8d7cc76b1d4489", null ],
    [ "SetPoints", "class_multi_region.html#a322a8d9c0e552cdf2a16a173882419a3", null ],
    [ "SetVertex", "class_multi_region.html#a7e23363ad6c78d46a54b25d99cbc890c", null ]
];