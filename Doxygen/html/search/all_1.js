var searchData=
[
  ['calccrosspoint',['CalcCrossPoint',['../d6/d9a/class_calculation.html#ab60390de35fc7d28b32e10b2387d7f03',1,'Calculation']]],
  ['calclength',['CalcLength',['../d6/d9a/class_calculation.html#a4bec8aac704a380e0f12163514f15666',1,'Calculation']]],
  ['calclineequation',['CalcLineEquation',['../d6/d9a/class_calculation.html#a4a4ac5f571e3ea0ccd5c6609b1e99fd7',1,'Calculation']]],
  ['calcperpendicular',['CalcPerpendicular',['../d6/d9a/class_calculation.html#a6c16b0a4d36ee342b7b748582b2a90b0',1,'Calculation']]],
  ['calculate',['Calculate',['../d9/d59/class_angle_between_lines.html#ac332bbf89c650e153b133abf2c8a6f1b',1,'AngleBetweenLines::Calculate()'],['../df/d16/class_distance_between_centers.html#ac0aa9fe1364c7fe21ccc9a06b95edd5a',1,'DistanceBetweenCenters::Calculate()'],['../dd/d22/class_distance_between_lines.html#aa0806e8ceebb6cb5829a6d96dd32d99c',1,'DistanceBetweenLines::Calculate()'],['../d1/d06/class_t_measurement.html#a4124bc507657d741cedc0156851ed22e',1,'TMeasurement::Calculate()'],['../d7/d07/class_t_ellipse.html#a7e6e8838e59328e7da3cc8cc180ee670',1,'TEllipse::Calculate()']]],
  ['calculateall',['CalculateAll',['../d1/d06/class_t_measurement.html#aae130bebbbbd5e35c43d9ed9fa4d4a98',1,'TMeasurement']]],
  ['calculateshapes',['CalculateShapes',['../d1/d06/class_t_measurement.html#ae106f443c1c5e298df20ce5becae18d8',1,'TMeasurement']]],
  ['calculation',['Calculation',['../d6/d9a/class_calculation.html',1,'Calculation'],['../d6/d9a/class_calculation.html#a6b3489cadb4ff8b31287d53944c34585',1,'Calculation::Calculation()']]],
  ['calibration',['Calibration',['../d2/d40/class_calibration.html',1,'']]],
  ['clear',['Clear',['../d1/d3a/class_raw_data.html#a561f4df963e685ad2a419166ff57ad17',1,'RawData']]],
  ['connect',['Connect',['../df/db7/class_t_data_stream.html#a049fde8aa6a6765fd8dc4efb1a1bc157',1,'TDataStream']]],
  ['createregion',['CreateRegion',['../d1/d06/class_t_measurement.html#a0cbdc482e8175265ffa5a1c01af03c73',1,'TMeasurement']]],
  ['createshape',['CreateShape',['../d1/d06/class_t_measurement.html#ac6209a73470dbe9843487d61d7c1144b',1,'TMeasurement']]],
  ['createspecifiedregion',['CreateSpecifiedRegion',['../d1/d06/class_t_measurement.html#a23f10bded8e724e659f59957229bedb0',1,'TMeasurement']]]
];
