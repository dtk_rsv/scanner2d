var class_calibration =
[
    [ "Calibration", "d2/d40/class_calibration.html#a80f51a5ff7ec0f44d5388c9a61d1f20b", null ],
    [ "GetNormalizedMean", "d2/d40/class_calibration.html#a6c58ddb39129bc80cd60693707e94c58", null ],
    [ "GetNormalizedStd", "d2/d40/class_calibration.html#ad4f3642f7b4267ebc4dd3914d5d4d557", null ],
    [ "GetOffsetX", "d2/d40/class_calibration.html#a9da9bfcacbec7636cd7727ce0c12a7c2", null ],
    [ "GetOffsetY", "d2/d40/class_calibration.html#a77b6e547ec0bb691271dc0d6db6b35d0", null ],
    [ "GetPoly2Coeff", "d2/d40/class_calibration.html#abdd45593407ef4b27a7baeec93affe2e", null ],
    [ "GetPoly9Coeff", "d2/d40/class_calibration.html#ac25db735c590ee12f10eb359d760e0b0", null ],
    [ "GetReflection", "d2/d40/class_calibration.html#ae3046f377aa25afc2697aa2a456244f6", null ],
    [ "Init", "d2/d40/class_calibration.html#a0f20051529b7e5142d1b94e2da9b86d3", null ],
    [ "Poly2", "d2/d40/class_calibration.html#a568e7a5c6f3795e37dfe759bfd819fb7", null ],
    [ "Poly9", "d2/d40/class_calibration.html#ad43c5ab182ec91efc425854fda267ce6", null ]
];