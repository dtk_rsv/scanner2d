var class_free_form =
[
    [ "FreeForm", "class_free_form.html#af01f8dc2bd7d788bddad83fab8ae7b59", null ],
    [ "~FreeForm", "class_free_form.html#a3cabdf0bb8a56debe8a0cbd5e9a80c1a", null ],
    [ "GetPointsInside", "class_free_form.html#a4be4293183914bd09fdbe03fdd6a7dfd", null ],
    [ "GetPointsInside", "class_free_form.html#afb473724cd74964dc46e4ff2824a51ef", null ],
    [ "GetRegionBoundariesX", "class_free_form.html#a3b27fd734b71e64661fe5c5839dc9ae1", null ],
    [ "MakeSelection", "class_free_form.html#a587dd294fa291f570fe5fe3973043363", null ],
    [ "MouseDown", "class_free_form.html#a75edb9754a2c97a9967b9d8d527c6830", null ],
    [ "MouseMove", "class_free_form.html#ab94bac07afef8ad59bde37fe523c23f1", null ],
    [ "RemoveSelection", "class_free_form.html#a5f376680c64b376d4c4d18b385233539", null ],
    [ "SetPoints", "class_free_form.html#aa19e4839a27b16ea4953abc216bd5015", null ],
    [ "SetVertex", "class_free_form.html#acc52d11f9fc0235ea397c8dd2149465a", null ]
];