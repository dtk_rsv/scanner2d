var class_line =
[
    [ "Line", "class_line.html#a9c8bf6fb6dd958ac94a4e4c39f74ef9e", null ],
    [ "~Line", "class_line.html#aabe85f48d22d92b62257091f48174fac", null ],
    [ "Calculate", "class_line.html#a647a20703a210d46b233f7c34e14570c", null ],
    [ "GetResults", "class_line.html#af059bc6e2738c504ae5f66e2b390d0b5", null ],
    [ "Hide", "class_line.html#a09da68992681c93bc5bc37a1c01db55c", null ],
    [ "MakeSelection", "class_line.html#af8e56b0ace9b5ec82c30912b7960f018", null ],
    [ "RemoveSelection", "class_line.html#a0734c5db3c4d1033adbbd763004d2ba7", null ],
    [ "Show", "class_line.html#a0b7f5184d4e5fc5dfd3ac5e9ee8fc9e3", null ]
];