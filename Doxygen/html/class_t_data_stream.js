var class_t_data_stream =
[
    [ "THREADMODE", "class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caef", [
      [ "THREADMODE_MEASUREMENT", "class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefa51dd9ca761448ff5d68f0f02d6fd613d", null ],
      [ "THREADMODE_VIDEO", "class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefac1708be407c3e23709401de27a9480e2", null ]
    ] ],
    [ "TDataStream", "class_t_data_stream.html#a9de616e1f0939bd1c5ca583d80aa54cb", null ],
    [ "Clear", "class_t_data_stream.html#a0d7a2a12cd33c78e091d4caedef04e2c", null ],
    [ "Execute", "class_t_data_stream.html#adb8954298dc9f7044d6cd660300864a7", null ],
    [ "Export", "class_t_data_stream.html#ac8924c479d1dc71b5b1baf6529db420c", null ],
    [ "ResetView", "class_t_data_stream.html#a048130ea47694977a680522155672084", null ],
    [ "SetMode", "class_t_data_stream.html#ae12868a6b53d3d4a41affca671beea9a", null ],
    [ "UpdGraphics", "class_t_data_stream.html#a5bf90f405756eb340bb58c75953345d9", null ]
];