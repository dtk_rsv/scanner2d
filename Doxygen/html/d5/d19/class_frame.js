var class_frame =
[
    [ "Frame", "d5/d19/class_frame.html#aa234809ba8714befa85a1226d5758bb2", null ],
    [ "~Frame", "d5/d19/class_frame.html#abec8c7bccdfc88cb4da137caae9f73d6", null ],
    [ "GetPointsInside", "d5/d19/class_frame.html#a3e1a70e1501a66e38f7a64a42f8fae3a", null ],
    [ "GetPointsInside", "d5/d19/class_frame.html#a900543efc47d0d11e1800aa4840cf954", null ],
    [ "GetRegionBoundariesX", "d5/d19/class_frame.html#afc035ab851cdec23f609c190e8a9bca0", null ],
    [ "Hide", "d5/d19/class_frame.html#a40dfad0f14a8c7f273417d2158d9afc4", null ],
    [ "KeyDown", "d5/d19/class_frame.html#a157ed635e8428083e0e88ca4e4faef2c", null ],
    [ "MakeSelection", "d5/d19/class_frame.html#a91d833e855c3d39a8d3025de18a32cf6", null ],
    [ "MouseDown", "d5/d19/class_frame.html#a252a49791ce515163bb0a93d386a2054", null ],
    [ "MouseMove", "d5/d19/class_frame.html#a53f8dc7a6bfc95ff02e5ed292108f47d", null ],
    [ "RemoveSelection", "d5/d19/class_frame.html#ae584dcd8ef0236853538915ccb08b071", null ],
    [ "SetVertex", "d5/d19/class_frame.html#a26f490c02bf47efa86ee8d0780d5ee01", null ],
    [ "Show", "d5/d19/class_frame.html#aa40e7d8afba39c86f9ff1de5c0523d8f", null ]
];