var class_shape =
[
    [ "SHAPETYPE", "d7/da7/class_shape.html#a3e1d7ab4625ce0f07032c628ed98cf0f", [
      [ "LINE", "d7/da7/class_shape.html#a3e1d7ab4625ce0f07032c628ed98cf0fac35e4c7790b51e8877af1fec04d42655", null ],
      [ "CIRCLE", "d7/da7/class_shape.html#a3e1d7ab4625ce0f07032c628ed98cf0fa341dd3322d286ad0dcd7949f9e13f1c3", null ],
      [ "ELLIPSE", "d7/da7/class_shape.html#a3e1d7ab4625ce0f07032c628ed98cf0fae6764a4a36b2096926d6b4800bb4d463", null ]
    ] ],
    [ "Shape", "d7/da7/class_shape.html#ab5013a61a1a434d580df3d9a9ab592f7", null ],
    [ "~Shape", "d7/da7/class_shape.html#a935afc9e576015f967d90de56977167d", null ],
    [ "Assign", "d7/da7/class_shape.html#a77f017aee6a089754f37ca3c8afde66e", null ],
    [ "Calculate", "d7/da7/class_shape.html#a1795c970442301e3e73446632b8bdc64", null ],
    [ "Clear", "d7/da7/class_shape.html#a612f6b23c6317cdc7e21566329160cfa", null ],
    [ "GetFullName", "d7/da7/class_shape.html#a1421f896277d93ae5e9055d445507c5f", null ],
    [ "GetIndex", "d7/da7/class_shape.html#a2e44fc31fc1a983ab08e936366b55f8b", null ],
    [ "GetRegionAndShapeType", "d7/da7/class_shape.html#a2ef50c22fc23e507bbf46146f3e79be2", null ],
    [ "GetResults", "d7/da7/class_shape.html#a2df16ac3677ee54bea489636f6f0f2c2", null ],
    [ "Hide", "d7/da7/class_shape.html#a125fd1d186f4e75615cccac3018dac14", null ],
    [ "MakeSelection", "d7/da7/class_shape.html#ad89830af8d7696d515095bf68e1ca2ef", null ],
    [ "RemoveSelection", "d7/da7/class_shape.html#a92d28c73600f2d2b64335a049459de69", null ],
    [ "Show", "d7/da7/class_shape.html#a0469239c2ccc57f5423933f76c26664c", null ],
    [ "Update", "d7/da7/class_shape.html#a60b0acbf79ccf3c099c5666606d50126", null ],
    [ "_color", "d7/da7/class_shape.html#af25f846a6a62629d673f2476980085f7", null ],
    [ "_series", "d7/da7/class_shape.html#a5e0c29a5fdaf70b81bf36c9305fac777", null ],
    [ "graph", "d7/da7/class_shape.html#a58d4db90cf29594466d773bcd4ca274e", null ],
    [ "name", "d7/da7/class_shape.html#afcf3b83c2ab303d26b2d2c2ea616d495", null ],
    [ "region", "d7/da7/class_shape.html#ab16329d6b0863437fb9e3f48178b169f", null ],
    [ "type", "d7/da7/class_shape.html#ad3f3f2691c543cf71f553b4860eeb12a", null ]
];