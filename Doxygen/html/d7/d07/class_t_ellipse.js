var class_t_ellipse =
[
    [ "TEllipse", "d7/d07/class_t_ellipse.html#a9a8a36022459276a6df10f14c5b01852", null ],
    [ "~TEllipse", "d7/d07/class_t_ellipse.html#a50bc0df2f4bf4ee15a43927d9b75575b", null ],
    [ "Assign", "d7/d07/class_t_ellipse.html#a53d70c2f54c20f43e35fda7c889058b3", null ],
    [ "Calculate", "d7/d07/class_t_ellipse.html#a7e6e8838e59328e7da3cc8cc180ee670", null ],
    [ "Clear", "d7/d07/class_t_ellipse.html#a90b793680b9b43ffa4eb55d416b28693", null ],
    [ "GetFullName", "d7/d07/class_t_ellipse.html#aec5a2732af4abf75bfabcee0723f6ef9", null ],
    [ "GetIndex", "d7/d07/class_t_ellipse.html#aa48baed63f75ac37ff50a24eb10477c9", null ],
    [ "GetResults", "d7/d07/class_t_ellipse.html#aa54f3a699e3505bbec98fe34cd5145f7", null ],
    [ "Update", "d7/d07/class_t_ellipse.html#a43b27b319cd763ce10ec679954a4fc2a", null ]
];