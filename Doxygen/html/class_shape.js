var class_shape =
[
    [ "Shape", "class_shape.html#a350ae8ab0f49adaf5262d7ae3542bd09", null ],
    [ "~Shape", "class_shape.html#a935afc9e576015f967d90de56977167d", null ],
    [ "Calculate", "class_shape.html#a1795c970442301e3e73446632b8bdc64", null ],
    [ "GetResults", "class_shape.html#a2df16ac3677ee54bea489636f6f0f2c2", null ],
    [ "Hide", "class_shape.html#a9b097410d6275c23c32d9a7ed8ac8406", null ],
    [ "MakeSelection", "class_shape.html#a0af7bcefd7e3d7403b29afe7f8dde37c", null ],
    [ "RemoveSelection", "class_shape.html#a1cf6f3c689ac910df175acf4bd24d29a", null ],
    [ "Show", "class_shape.html#af3d79a0640123cf9752ac77f96a3dd1d", null ],
    [ "graph", "class_shape.html#a58d4db90cf29594466d773bcd4ca274e", null ],
    [ "name", "class_shape.html#afcf3b83c2ab303d26b2d2c2ea616d495", null ],
    [ "region", "class_shape.html#ab16329d6b0863437fb9e3f48178b169f", null ]
];