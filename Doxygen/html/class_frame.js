var class_frame =
[
    [ "Frame", "class_frame.html#aa234809ba8714befa85a1226d5758bb2", null ],
    [ "~Frame", "class_frame.html#abec8c7bccdfc88cb4da137caae9f73d6", null ],
    [ "GetPointsInside", "class_frame.html#a3e1a70e1501a66e38f7a64a42f8fae3a", null ],
    [ "GetPointsInside", "class_frame.html#a900543efc47d0d11e1800aa4840cf954", null ],
    [ "GetRegionBoundariesX", "class_frame.html#afc035ab851cdec23f609c190e8a9bca0", null ],
    [ "MakeSelection", "class_frame.html#a2044167c5783b716f6f33bbe17f58ee2", null ],
    [ "MouseDown", "class_frame.html#a252a49791ce515163bb0a93d386a2054", null ],
    [ "MouseMove", "class_frame.html#a53f8dc7a6bfc95ff02e5ed292108f47d", null ],
    [ "RemoveSelection", "class_frame.html#ac1100fc2f1565989db1b946ec5c9f06b", null ],
    [ "SetPoints", "class_frame.html#a3c00fba85e71638c045c21b26dd041e5", null ],
    [ "SetVertex", "class_frame.html#a26f490c02bf47efa86ee8d0780d5ee01", null ]
];