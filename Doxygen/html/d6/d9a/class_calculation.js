var class_calculation =
[
    [ "CALCTYPE", "d6/d9a/class_calculation.html#a69e8bffd55f8dcefd0c32818011429ff", [
      [ "ANGLE_BETWEEN_LINES", "d6/d9a/class_calculation.html#a69e8bffd55f8dcefd0c32818011429ffae45313d1c63d305ca07f99fd6e8e46e2", null ],
      [ "DISTANCE_BETWEEN_CENTERS", "d6/d9a/class_calculation.html#a69e8bffd55f8dcefd0c32818011429ffa132e3ca9ff18e8571eba3604096ca80c", null ],
      [ "DISTANCE_BETWEEN_LINES", "d6/d9a/class_calculation.html#a69e8bffd55f8dcefd0c32818011429ffaecee28f6f9530f9d2b2ceb29b9c128ba", null ]
    ] ],
    [ "Calculation", "d6/d9a/class_calculation.html#a6b3489cadb4ff8b31287d53944c34585", null ],
    [ "~Calculation", "d6/d9a/class_calculation.html#a5ed26896b156b8a0d494302a5ccd5e80", null ],
    [ "Calculate", "d6/d9a/class_calculation.html#a04d08092aad8299e952328cedb11484c", null ],
    [ "GetName", "d6/d9a/class_calculation.html#a3436d1efe00ecb910fb7ceb74bd9f6cc", null ],
    [ "GetNames", "d6/d9a/class_calculation.html#a6658bc420346765442533499dd8bf5c8", null ],
    [ "GetResult", "d6/d9a/class_calculation.html#a7f762953880533b02872d70f3a30a5ac", null ],
    [ "GetShapes", "d6/d9a/class_calculation.html#a26542ae9982a9c6e32c57f6869809c12", null ],
    [ "GetSize", "d6/d9a/class_calculation.html#ab563d29bf1e301a0c9b57991f80a3c82", null ],
    [ "HideAux", "d6/d9a/class_calculation.html#a2c4e891f448b8004bb366d44fb03c57c", null ],
    [ "HideMarks", "d6/d9a/class_calculation.html#aaf135d1173848c506a18360a97bc00a9", null ],
    [ "MakeSelection", "d6/d9a/class_calculation.html#a3c663a4cafc395d7c1dc44596defd523", null ],
    [ "RemoveSelection", "d6/d9a/class_calculation.html#a0fa0b00ef3768cdd24b943116e3637b5", null ],
    [ "ShowAux", "d6/d9a/class_calculation.html#ae57b97e841ea900afa95807226398645", null ],
    [ "ShowMarks", "d6/d9a/class_calculation.html#afb8c60dda2109da183c40d2eda697770", null ],
    [ "graph", "d6/d9a/class_calculation.html#a41d5b1c5bedeef1437b1961230913d76", null ],
    [ "name", "d6/d9a/class_calculation.html#a73993f0756e2b1530dd73e94037d367a", null ],
    [ "result", "d6/d9a/class_calculation.html#a928fe74f97cbc664549f79602ef5ad57", null ],
    [ "series", "d6/d9a/class_calculation.html#aaab6846160f7c82d1948ce3f133fe8e9", null ],
    [ "shapes", "d6/d9a/class_calculation.html#a68ca0a81db6c1ee3cc26a756099f3283", null ]
];