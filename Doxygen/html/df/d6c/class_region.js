var class_region =
[
    [ "REGIONTYPE", "df/d6c/class_region.html#a4282a527531780498ccc798dc4b01eb1", [
      [ "FRAME", "df/d6c/class_region.html#a4282a527531780498ccc798dc4b01eb1a51e1c8ff13b89c38327fdbd75f0203d9", null ],
      [ "TRIANGLE", "df/d6c/class_region.html#a4282a527531780498ccc798dc4b01eb1af96e320d57ab7dddea995d29315c6b0d", null ],
      [ "POLYGON", "df/d6c/class_region.html#a4282a527531780498ccc798dc4b01eb1a6cf26b0975d92db3e1fab8a2e0c4b593", null ],
      [ "UNION", "df/d6c/class_region.html#a4282a527531780498ccc798dc4b01eb1a40c33eeea974a713458ba263391f36e1", null ]
    ] ],
    [ "Region", "df/d6c/class_region.html#a5637c5dd01582f3ad968fcd055652db6", null ],
    [ "~Region", "df/d6c/class_region.html#a3c3670fff78f7511d156e3b2f0bc6266", null ],
    [ "GetDataBoundariesX", "df/d6c/class_region.html#a0e8f53b1de1396cc8056832e3bb10154", null ],
    [ "GetName", "df/d6c/class_region.html#abe6c4e51c83ff458b146a2aa27287733", null ],
    [ "GetPointsInside", "df/d6c/class_region.html#a22b4e248b4d0e8f7071ec536ad2ac51a", null ],
    [ "GetPointsInside", "df/d6c/class_region.html#af0099aceff3554e0f79793526c9e4e53", null ],
    [ "GetRegionBoundariesX", "df/d6c/class_region.html#ae4616b54d730eb62fc94c5ee2c6f357b", null ],
    [ "GetType", "df/d6c/class_region.html#aa7efc9fbf8ae93e9b343b0700cdbc78a", null ],
    [ "Hide", "df/d6c/class_region.html#af67e54f14937779863ec0aa957a9c721", null ],
    [ "KeyDown", "df/d6c/class_region.html#a0547528ebd1fc1a471d1d5655ec2f200", null ],
    [ "MakeSelection", "df/d6c/class_region.html#a742bc493fdd8408cbe705b323e02ccd1", null ],
    [ "MouseDown", "df/d6c/class_region.html#a6d951f2174a9ccef26abb6566577991c", null ],
    [ "MouseMove", "df/d6c/class_region.html#a872504f166324ac0104a6d1a21722932", null ],
    [ "RemoveSelection", "df/d6c/class_region.html#a21be07810b11199cf3062575847741d5", null ],
    [ "SetVertex", "df/d6c/class_region.html#aa73f04930c7dfab81481b501a03d095f", null ],
    [ "Show", "df/d6c/class_region.html#abd3629a83b52233a296f390c52541a28", null ],
    [ "count", "df/d6c/class_region.html#a07fbb7bc87c40908c9c14f885e932d84", null ],
    [ "data", "df/d6c/class_region.html#a194ec9b5552c4e1f404db950c9c0ef3b", null ],
    [ "graph", "df/d6c/class_region.html#a1c2a60ae277eee0b99ce1d025ee3a41f", null ],
    [ "myShapes", "df/d6c/class_region.html#a0baea5dab7de544f748234ad588a0a52", null ],
    [ "series", "df/d6c/class_region.html#ac25400fa24c082d5adab43135e4f62c1", null ]
];