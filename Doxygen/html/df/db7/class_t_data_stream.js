var class_t_data_stream =
[
    [ "THREADMODE", "df/db7/class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caef", [
      [ "THREADMODE_MEASUREMENT", "df/db7/class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefa51dd9ca761448ff5d68f0f02d6fd613d", null ],
      [ "THREADMODE_SHOWHYSTOGRAM", "df/db7/class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefad870ddae2a2b18a48f01d554adad84c6", null ],
      [ "THREADMODE_SHOWPOINTS", "df/db7/class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefabcbde5de8ec193f6525c04b2e195a4af", null ],
      [ "THREADMODE_SHOWCIRCLES", "df/db7/class_t_data_stream.html#a30cd1a3c6f185e891338d931d411caefa420b0bcc38655afd4e5eb94b1d712c64", null ]
    ] ],
    [ "TDataStream", "df/db7/class_t_data_stream.html#a82e5af0de950cf9ac83c819797039756", null ],
    [ "~TDataStream", "df/db7/class_t_data_stream.html#a34ad84b71d0868274a76bd19b32df0ac", null ],
    [ "Connect", "df/db7/class_t_data_stream.html#a049fde8aa6a6765fd8dc4efb1a1bc157", null ],
    [ "Disconnect", "df/db7/class_t_data_stream.html#ac445020ac005aca50b5288485d616686", null ],
    [ "Execute", "df/db7/class_t_data_stream.html#adb8954298dc9f7044d6cd660300864a7", null ],
    [ "Export", "df/db7/class_t_data_stream.html#ac8924c479d1dc71b5b1baf6529db420c", null ],
    [ "IsConnected", "df/db7/class_t_data_stream.html#a800b40f03f64373a5826377e03a7747e", null ],
    [ "SetMode", "df/db7/class_t_data_stream.html#ae12868a6b53d3d4a41affca671beea9a", null ]
];